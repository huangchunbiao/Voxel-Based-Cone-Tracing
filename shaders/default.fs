#version 420 core

in vec2 vert_uv;
in vec3 vert_normal;
in vec4 vert_possition;
in vec4 vert_light_position;

out vec4 color;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;
uniform sampler2D shadow_map;

uniform vec3 col;
uniform int hasTexture;

float shadow_calculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadow_map, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.01;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}


void main(){
  float shadow = shadow_calculation(vert_light_position);

  if (hasTexture == 0) {
    color = vec4(col, 1.0);
  } else {
    vec4 c = texture(diffuse, vert_uv);
    if (c.a < 0.1) discard;

    //float angle = max(dot(vert_normal, normalize(vec3(.1, 1., .1))), 0.);
    //color = vec4(c.xyz * angle, c.w);
    color = c;
  }

  color.rgb *= 0.5 * (1.0 - shadow) + 0.5;
}
