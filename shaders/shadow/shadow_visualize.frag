#version 330 core

out vec4 color;
in vec2 texture_coord;

uniform sampler2D depth_map;

void main(){
  float depth_value = texture2D(depth_map, (texture_coord + 1) / 2.0).x;
    // FragColor = vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0); // perspective
  color = vec4(vec3(depth_value), 1.0);
  //color = vec4(vec3(depth_value), 1.0); // orthographic
}
