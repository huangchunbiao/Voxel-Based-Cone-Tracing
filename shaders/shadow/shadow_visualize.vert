#version 330 core

layout (location = 0) in vec3 vertex;

out vec2 texture_coord;

void main(){
  texture_coord = vertex.xy;
  gl_Position = vec4(vertex,1.0);
}
