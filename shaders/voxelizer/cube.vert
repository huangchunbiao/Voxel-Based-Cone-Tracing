#version 420 core

layout (location = 0) in vec3 vertex;

uniform vec3 color;
uniform vec3 cube_center;

uniform mat4 view_mat;
uniform mat4 projection_mat;

out vec3 frag_color;

void main(){
  frag_color = color;
  vec3 scale = vec3(1/512, 1/512, 1/512);
  vec4 pos_t = vec4(scale * vertex + cube_center, 1.0);
  gl_Position = projection_mat * view_mat * pos_t;
}
