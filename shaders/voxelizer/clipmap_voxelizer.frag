#version 450 core

#define MAX_POINT_LIGHT 10
#define MAX_DIRECTIONAL_LIGHT 5

struct PointLight_t {
  vec3 position; //offset 0
  vec3 color; //offset 16
  float intensity; //offset 32
};

struct DirectionalLight_t {
  vec3 direction; //offset 0
  vec3 color; //offset 16
  float intensity; //offset 32
};

/*
layout (std140) uniform PointLightBlock {
  PointLight_t PointLights[MAX_POINT_LIGHT];
};
uniform int PointLightCount;

layout (std140) uniform DirectionalLightBlock {
  DirectionalLight_t DirectionalLights[MAX_POINT_LIGHT];
};
uniform int DirectionalLightCount;
*/

in vec2 geom_uv;
in vec3 geom_normal;
in vec3 geom_position;
in vec3 geom_position_proj;
in vec4 geom_position_light_space;

layout (r32ui) uniform coherent volatile uimage3D voxel_field;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;
uniform sampler2D shadow_map;

uniform int level;
uniform int clipmap_levels;
uniform int resolution;

uniform vec3 camera_pos;

uniform vec3 emission;

vec4 diffuse_c = texture(diffuse, geom_uv);
vec4 albedo_c = texture(albedo, geom_uv);
vec4 normal_c = texture(normal_map, geom_uv);
vec4 specular_c = texture(specular, geom_uv);

vec4
convRGBA8ToVec4(uint val){
  return  vec4( float( (val&0x000000FF) ),
		float( (val&0x0000FF00) >>8U ),
		float( (val&0x00FF0000) >>16U ),
		float( (val&0xFF000000) >>24U ) );
}

uint convVec4ToRGBA8(vec4 val) {
  return ( uint( val.w )&0x000000FF )  <<24U |
	 ( uint( val.z )&0x000000FF )  <<16U |
	 ( uint( val.y )&0x000000FF )  << 8U |
	 ( uint( val.x )&0x000000FF );
}

void imageAtomicRGBA8Avg(layout(r32ui) coherent volatile uimage3D imgUI, ivec3 coords, vec4 val) {
  val.rgb *= 255.0f;
  //Optimise  following calculations
  uint newVal = convVec4ToRGBA8(val);
  uint prevStoredVal = 0;
  uint curStoredVal;
  //Loop as  long as  destination value  gets changed  by  other  thr eads
  while ( (curStoredVal = imageAtomicCompSwap(imgUI , coords , prevStoredVal , newVal)) !=  prevStoredVal) {
    prevStoredVal = curStoredVal;
    vec4 rval=convRGBA8ToVec4(curStoredVal);

    rval.xyz=(rval.xyz*rval.w);
    // Denormalize
    vec4 curValF =rval+val;
    //Add new value
    curValF.xyz/=(curValF.w);
    // Renormalize
    newVal = convVec4ToRGBA8(curValF);
  }
}

vec3 calculate_light(vec3 light_dir, vec3 light_color, float light_intensity,
		     bool attenuation) {
  float light_distance = length(light_dir);

  float NdotL = dot(normalize(geom_normal), normalize(light_dir));
  float intensity = max(NdotL, 0.);
  vec3 color = albedo_c.xyz *  intensity * light_color * light_intensity;

  if (attenuation)
    color /= light_distance / 100.+ 1;

  return color;
}

vec3 shade() {
  vec3 color = vec3(0);

  const int DirectionalLightCount = 3;
  const int PointLightCount = 1;

  DirectionalLight_t DirectionalLights[DirectionalLightCount];
  DirectionalLights[0] = DirectionalLight_t(vec3(0., 300., 0),
					    vec3(1.),
					    10.);


  PointLight_t PointLights[PointLightCount];
  PointLights[0] = PointLight_t(vec3(0, 300., 0), vec3(1.), 1.);

  for (int i = 0; i < PointLightCount; i++) {
    PointLight_t ls = PointLights[i];
    vec3 light_dir = ls.position - geom_position;
    color += calculate_light(light_dir, ls.color, ls.intensity, true);
  }

  return color;
  for (int i = 0; i < DirectionalLightCount; i++) {
    DirectionalLight_t ls = DirectionalLights[i];
    vec3 light_dir = -ls.direction;
    color += calculate_light(light_dir, ls.color, ls.intensity, false);
  }

  return color;
}

float shadow() {
  vec3 p_l = geom_position_light_space.xyz / geom_position_light_space.w;
  p_l = p_l * 0.5 + 0.5;
  float map_depth = texture(shadow_map, p_l.xy).r;

  float bias = max(0.05 * (1.0 - dot(geom_normal, normalize(vec3(0, 2000, 250)))), 0.005);
  return p_l.z - bias > map_depth ? 0. : 1.;
}

void main(){
  vec4 res = vec4(shade(), 1.0);
  res.xyz = albedo_c.xyz * shadow() * max(0, dot(normalize(vec3(0, 2000, 250)), geom_normal));
  if (emission != vec3(0)) {
    res.xyz = 0.5 * emission;
  }

  ivec3 pos = ivec3(resolution * (0.5 * geom_position_proj + 0.5));
  pos.y += (resolution + 2) * level;

  vec3 p = abs(geom_position_proj);
  if (max(p.x, max(p.y, p.z)) > 1.) discard;

  //res = vec4(albedo_c.xyz, 1.);
  imageAtomicRGBA8Avg(voxel_field, pos, res);
}
