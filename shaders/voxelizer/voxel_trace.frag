#version 420 core


uniform vec3 camera_pos;
uniform vec3 camera_dir;
uniform vec3 camera_up;
uniform vec3 world_box;


uniform float fov_tan;
uniform float aspect_ratio;
uniform vec2 resolution;

uniform ivec3 voxel_texture_size;

uniform sampler3D voxel_field;

//layout (rgba8, binding = 1) uniform sampler3D voxel_field;

out vec4 color;
in vec3 position;

ivec3 to_voxel_coord(vec3 pos){
  pos /= 2048.0;
  pos = pos * 0.5 + 0.5;
  return ivec3(pos*512);
}

bool is_outside(ivec3 pos){
  return (pos.x > 512 || pos.y > 512 || pos.z > 512 || pos.x < 0 || pos.y < 0 || pos.z < 0);
}

float calculate_step(vec3 pos, vec3 r, float level){
  float cube_side =  2 * world_box.x / (512.0 / pow(2,level));
  vec3 cube_coord = mod(pos, cube_side);

  float t = 1337;

  for(int i = 0; i < 3; i++){
    float cs = r[i] < 0 ? 0 : cube_side;
    float dt = (cs - cube_coord[i]) / r[i];
    t = min(t, dt);
  }
  return t;
}


vec4 sample_step(vec3 pos, vec3 r, int max_level, out float t){
  vec4 value;
  for(int level = max_level; level >= 0; level--){
    vec3 texture_coord = pos / (world_box * 2) + 0.5;
    value = textureLod(voxel_field, texture_coord, level);

    if(value == vec4(0)){
      t = calculate_step(pos, r, float(level));
      return vec4(0);
    }

  }
  t = calculate_step(pos, r, 0.0);
  return value;
}

void main(){

  vec3 span_x = cross(camera_dir, camera_up);

  vec2 uv = gl_FragCoord.xy / resolution;
  uv = uv * 2.0 - 1.0;

  vec3 plane_coord = span_x * uv.x + camera_up * uv.y;
  //plane_coord *= 50;
  plane_coord += camera_pos + camera_dir;

  vec3 r = normalize(plane_coord - camera_pos);

  float t = 1.;

  vec3 cur_pos = camera_pos;
  float  level = 1.0;
  for(;;) {
    ivec3 tex_coord = to_voxel_coord(cur_pos);
    if(is_outside(tex_coord)){
      color = vec4(1.0);
      break;
      discard;
    }
    //    vec3 texture_coord = cur_pos / ( world_box * 2) + 0.5;
    vec4 voxel = sample_step(cur_pos, r, 4, t);// textureLod(voxel_field, texture_coord, level);
    if(voxel != vec4(0)){
      color = vec4(voxel.xyz,1.0);
      break;
    }
    cur_pos += (t + 0.05) * r;
  }
}
