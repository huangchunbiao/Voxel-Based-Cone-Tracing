#version 450 core


struct VoxelData {
  uint position;
  uint color;
};
layout (std430, binding = 0) buffer voxel_store{
  VoxelData voxel_list[];
};

layout (binding = 1) uniform atomic_uint voxel_count;


uniform uint voxel_size;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;
//uniform sampler2D shadow_map;

in vec2 geom_uv;
in vec3 geom_normal;
in vec3 geom_position;
in vec4 geom_light_position;

uint ivec3_to_uint32(ivec3 val){
  ivec3 t = (val & 0x000003FF) << ivec3(20,10,0);
  return t.x | t.y | t.z;
}

uint rgba8_to_uint32(vec4 val){
  uvec4 t = uvec4(val) << uvec4(24,16,8,0);
  return t.x | t.y | t.z | t.w;
}

void main(){

  vec4 color = texture(diffuse, geom_uv);
  // if(color.a < 0.1)
  //   discard;
  color.a = 1.0;
  //color = vec4(1,0,0,1);
  ivec3 voxel_position = ivec3(voxel_size * (0.5 * geom_position + 0.5));

  VoxelData data;
  data.position = ivec3_to_uint32(voxel_position);
  data.color   = rgba8_to_uint32(color * 255.0);

  uint list_position = atomicCounterIncrement(voxel_count);

  voxel_list[list_position] = data;
}
