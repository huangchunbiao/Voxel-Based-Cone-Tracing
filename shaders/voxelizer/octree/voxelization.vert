#version 420 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out vec2 vert_uv;
out vec3 vert_normal;
out vec3 vert_position;
//out vec4 vert_light_position;

uniform mat4 projection;
uniform mat4 model;
//uniform mat4 light_projection;

void main(){
  vert_uv = uv;
  vert_normal = normal;
  vec4 position = projection * model * vec4(vertex, 1);
  vert_position = position.xyz;
  //vert_light_position = light_projection * vec4(vertex, 1);

  gl_Position = position;
}
