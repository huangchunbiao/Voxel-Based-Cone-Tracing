#version 420 core
layout (location = 0) out vec3 tex_position;
layout (location = 1) out vec3 tex_normal;
layout (location = 2) out vec4 tex_albedo_spec;

in vec2 vert_uv;
in vec3 vert_normal;
in vec4 vert_position;
in vec4 vert_light_position;

out vec4 color;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;

uniform vec3 col;
uniform int hasTexture;

// float shadow_calculation(vec4 fragPosLightSpace)
// {
//     // perform perspective divide
//     vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
//     // transform to [0,1] range
//     projCoords = projCoords * 0.5 + 0.5;
//     // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
//     float closestDepth = texture(shadow_map, projCoords.xy).r;
//     // get depth of current fragment from light's perspective
//     float currentDepth = projCoords.z;
//     // check whether current frag pos is in shadow
//     float bias = 0.01;
//     float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

//     return shadow;
// }


void main(){

  tex_position = vert_position.xyz;
  tex_normal  = vert_normal;
  tex_albedo_spec = texture(diffuse, vert_uv);


}
