#version 460 core
#pragma optionNV (unroll all)

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;


struct OctreeNode {
  uint header;
  uint neighbor[6];
  uint parent_ptr;
};
layout (std430, binding = 0)  volatile buffer octree_node_store {
  OctreeNode octree_nodes[];
};
uniform layout (rgba8, binding = 1) volatile image3D voxel_brick_buffer;

layout (std430, binding = 2)  volatile buffer octree_node_level {
  int level_count[10];
  int buffer_offsets[10];
  uint level_buffer[];
};

uniform ivec3 voxel_bricks_dims;
uniform bool skip_static;
uniform int current_level;

const uint STATIC_BIT = 1 << 31;
const uint DYNAMIC_CURRENT_BIT = 1 << 30;
const uint DYNAMIC_PREV_BIT = 1 << 29;
const uint LIGHT_CURRENT_BIT = 1 << 28;
const uint LIGHT_PREV_BIT = 1 << 27;
// During construction  the light prev bit is used for marking nodes;
const uint SUBDIVIDE_BIT = LIGHT_PREV_BIT;
const uint CHILD_INDEX_BITS = 0x07FFFFFF;

// Node  idx shouldbe the child_ptr (start of every block)
ivec3 octree_node_brick_coord(uint node_idx){
  node_idx -= 1; // Ignoring the root node idx
  const ivec3 D = voxel_bricks_dims;
  return 3 * ((ivec3(node_idx) / ivec3(8, 8*D.x, 8*D.x*D.y)) % ivec3(D.x, D.y,D.z));
}

ivec3 idx_to_octant_vec(uint n){
  return (ivec3(n) >> ivec3(2,1,0)) & 1;
}

uint octree_node_child_idx(uint location){
  return octree_nodes[location].header & 0x07FFFFFF;
}

vec4 uint32_to_rgba8(uint v){
  return vec4((v >> uvec4(24,16,8,0)) & 255);
}

uint rgba8_to_uint32(vec4 val){
  uvec4 t = uvec4(val) << uvec4(24,16,8,0);
  return t.x | t.y | t.z | t.w;
}

void filter_leaf_block(uint block_start){
  const vec4 v0 = vec4(0);
  const vec4 init[3] = {v0,v0,v0};
  const vec4 init1[3][3] = {init,init,init};
  vec4 brick[3][3][3] = {init1,init1,init1};

  ivec3 brick_coord = octree_node_brick_coord(block_start);

  for(int octant = 0; octant < 8; octant++){
    if(octree_nodes[block_start+octant].header == 0)
      continue;
    vec4 color = uint32_to_rgba8(octree_nodes[block_start+octant].neighbor[0]) / 255.;
    vec4 radiance = uint32_to_rgba8(octree_nodes[block_start+octant].neighbor[1]) / 255.;
    //color.rgb *= radiance.rgb;
    ivec3 o_vec = idx_to_octant_vec(octant);
    for(int i = 0; i < 8; i++){
      ivec3 coord = o_vec + idx_to_octant_vec(i);
      brick[coord.x][coord.y][coord.z] += color;
    }
  }
  const mat3 side_kernel = mat3( 1., 0.5, 1.,
			   0.5, 0.25, 0.5,
			   1., 0.5, 1.);
  const mat3 center_kernel = mat3(0.5, 0.25, 0.5,
			    0.25, 0.125, 0.25,
			    0.5, 0.25, 0.5);
  const mat3 kernel[3] = {side_kernel, center_kernel, side_kernel};

  for(int x= 0; x < 3; x++){
    for(int y=0; y < 3; y++){
      for(int z=0; z < 3; z++){
	ivec3 coord = ivec3(x,y,z);
	float weight = kernel[x][y][z];
	vec4 color = brick[x][y][z];
	color *= weight;
	imageStore(voxel_brick_buffer, brick_coord + coord,
		   color);
      }
    }
  }
}

bool should_process(uint header){
  bool is_static = bool(header & STATIC_BIT);
  bool is_dynamic_current = bool(header & DYNAMIC_CURRENT_BIT);
  bool is_dynamic_prev = bool(header & DYNAMIC_PREV_BIT);
  bool is_light_current = bool(header & LIGHT_CURRENT_BIT);
  bool is_light_prev = bool(header & LIGHT_PREV_BIT);

  bool redo = is_dynamic_current || is_dynamic_prev;
  return redo || (!skip_static && is_static);
}

void main () {
  int buffer_offset = buffer_offsets[current_level];
  for(int level_buffer_node_idx = atomicAdd(level_count[current_level], -1) -1;
      level_buffer_node_idx >= 0;
      level_buffer_node_idx = atomicAdd(level_count[current_level], -1) - 1
      ){
    uint node_idx = level_buffer[buffer_offset + level_buffer_node_idx];
    uint child_idx_start = octree_node_child_idx(node_idx);

    if( should_process( octree_nodes[node_idx].header ) ){
      filter_leaf_block(child_idx_start);
    }
  }
}
