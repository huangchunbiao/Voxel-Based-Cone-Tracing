#version 420 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out vec2 vertex_uv;
out vec4 vertex_normal;
out vec4 world_possition;

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 projection_mat;

void main(){
  vertex_uv = uv;
  vec4 pos_t =  vec4(vertex, 1.0);
  gl_Position = projection_mat * view_mat * pos_t;
}
