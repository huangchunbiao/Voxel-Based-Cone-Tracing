#version 420 core
layout (location = 0) in vec3 vertex;


out vec2 vert_position;

void main(){  
  vert_position = vertex.xy * 0.5 + 0.5;
  gl_Position = vec4(vertex,1.0);
}
