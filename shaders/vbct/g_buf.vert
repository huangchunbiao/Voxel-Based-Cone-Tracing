#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out vec2 vert_uv;
out vec3 vert_normal;
out vec3 vert_tangent;
out vec3 vert_bitangent;
out vec4 vert_position;
out vec4 vert_light_position;

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 projection_mat;
uniform mat4 light_space_matrix;

void main(){

  vert_tangent = tangent;
  vert_bitangent = bitangent;
  
  vert_uv = uv;
  vert_normal = normal;
  vec4 pos_t = vec4(vertex, 1.0);
  vert_light_position = light_space_matrix * pos_t;
  vert_position = pos_t;
  gl_Position = projection_mat * view_mat * model_mat * pos_t;
}
