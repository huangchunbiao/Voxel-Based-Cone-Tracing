#version 420 core
layout (location = 0) out vec3 tex_position;
layout (location = 1) out vec3 tex_normal;
layout (location = 2) out vec4 tex_albedo_spec;
layout (location = 3) out vec3 tex_tangent;
layout (location = 4) out vec3 tex_emission;

in vec2 vert_uv;
in vec3 vert_normal;
in vec3 vert_tangent;
in vec3 vert_bitangent;
in vec4 vert_position;
in vec4 vert_light_position;

out vec4 color;

uniform sampler2D diffuse;
//uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;

uniform vec3 col;
uniform int hasTexture;

uniform vec3 emission;

// float shadow_calculation(vec4 fragPosLightSpace)
// {
//     // perform perspective divide
//     vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
//     // transform to [0,1] range
//     projCoords = projCoords * 0.5 + 0.5;
//     // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
//     float closestDepth = texture(shadow_map, projCoords.xy).r;
//     // get depth of current fragment from light's perspective
//     float currentDepth = projCoords.z;
//     // check whether current frag pos is in shadow
//     float bias = 0.01;
//     float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

//     return shadow;
// }


void main(){
  mat3 tangent_space_mat = mat3(vert_tangent, vert_bitangent, vert_normal);
  tex_tangent = vert_tangent * 0.5 + 0.5;

  tex_position = vert_position.xyz;

  vec3 normal = texture(normal_map, vert_uv).xyz;
  if (normal == vec3(0)) {
    tex_normal = vert_normal;
  } else {
    normal.xy = normal.xy * 2. - 1.;
    vec3 normal_offset = tangent_space_mat * normal;
    tex_normal = normalize(normal_offset);
  }

  tex_albedo_spec = texture(diffuse, vert_uv);
  if (tex_albedo_spec.a == 0.) discard;
  tex_albedo_spec.a = texture(specular, vert_uv).r;

  tex_emission = emission;

  //tex_albedo_spec.xyz = texture(normal_map, vert_uv).xyz;
}
