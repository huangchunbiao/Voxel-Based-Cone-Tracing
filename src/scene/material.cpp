#include <iostream>
#include <cassert>

#include <IL/il.h>
#include <IL/ilu.h>

#include "material.hpp"

Material::Material(aiMaterial* material, std::string textures_path, bool windows) {
  static bool INIT;
  if ( !INIT ) {
    INIT = true;
    ilInit();
    iluInit();
  }

  memset(texture_buffer_, 0, texture_t_n * sizeof(RawTexture));

  for (int i = aiTextureType_DIFFUSE; i < aiTextureType_LIGHTMAP; i++) {    
    if ( material->GetTextureCount( (aiTextureType) i ) > 0) {      
      aiString path;
      material->GetTexture((aiTextureType) i, 0, &path);

      std::string full_path = textures_path + std::string(path.data);
      std::cout << full_path << ": " << getTextureTypeString((texture_t)(i - 1)) << std::endl;

      if ( !windows ) {
	std::replace(full_path.begin(), full_path.end(), '\\', '/');
      }
      texture_names_[i - aiTextureType_DIFFUSE] = full_path;
      texture_buffer_[i - aiTextureType_DIFFUSE] = loadImage(full_path.c_str());
    } else {
      assert(texture_buffer_[i - aiTextureType_DIFFUSE].buffer == NULL);
    }
  }

  if (hasTexture(OPACITY_TEXTURE)) {
    RawTexture opacity = getTexture(OPACITY_TEXTURE);
    for (int i = 0; i < texture_t_n; i++) {
      if (i != OPACITY_TEXTURE) {
	RawTexture cur = getTexture((texture_t)i);
	for (int j = 0; j < cur.width * cur.height; j++) {
	  cur.buffer[j*4+3] = opacity.buffer[j*4];
	}
      }
    }
  }
}

Material::~Material() {
  for (int i = 0; i < texture_t_n; i++) {
    if (texture_buffer_[i].buffer != NULL) {
      delete texture_buffer_[i].buffer;
    }
  }
}

Material::Material(const Material& m) {
  for(int i = 0; i < texture_t_n; i++){
    texture_names_[i] = m.texture_names_[i];
  }

  for (int i = 0; i < texture_t_n; i++) {
    texture_buffer_[i].buffer = NULL;
    if (m.texture_buffer_[i].buffer != NULL) {
      int width = m.texture_buffer_[i].width;
      int height = m.texture_buffer_[i].height;
      int bytes_per_pixel = m.texture_buffer_[i].bytes_per_pixel;

      texture_buffer_[i].width = width;
      texture_buffer_[i].height = height;
      texture_buffer_[i].bytes_per_pixel = bytes_per_pixel;

      int buffer_size = width * height * bytes_per_pixel;
      texture_buffer_[i].buffer = new uint8_t[buffer_size];
      memcpy(texture_buffer_[i].buffer, m.texture_buffer_[i].buffer, buffer_size);
    } else {
      texture_buffer_[i].width = 0;
      texture_buffer_[i].height = 0;
      texture_buffer_[i].bytes_per_pixel = 0;
      texture_buffer_[i].buffer = NULL;
    }
  }
}

Material& Material::operator=(const Material& m) {
  for(int i = 0; i < texture_t_n; i++){
    texture_names_[i] = m.texture_names_[i];
  }

  for (int i = 0; i < texture_t_n; i++) {
    texture_buffer_[i].buffer = NULL;
    if (m.texture_buffer_[i].buffer != NULL) {
      int width = m.texture_buffer_[i].width;
      int height = m.texture_buffer_[i].height;
      int bytes_per_pixel = m.texture_buffer_[i].bytes_per_pixel;

      texture_buffer_[i].width = width;
      texture_buffer_[i].height = height;
      texture_buffer_[i].bytes_per_pixel = bytes_per_pixel;

      int buffer_size = width * height * bytes_per_pixel;
      texture_buffer_[i].buffer = new uint8_t[buffer_size];
      memcpy(texture_buffer_[i].buffer, m.texture_buffer_[i].buffer, buffer_size);
    }
  }
}

bool Material::hasTexture(texture_t type) {
  return texture_buffer_[type].buffer == NULL ? false : true;
}

RawTexture Material::getTexture(texture_t type) {
  return texture_buffer_[type];
}

RawTexture Material::loadImage(const char* filename) {
  RawTexture t;

  ILuint image;
  ilGenImages(1, &image);
  ilBindImage(image);

  ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

  if ( !ilLoadImage( filename ) ) {
    std::cerr << "Failed to load image: " << filename << std::endl;
    std::cerr << iluErrorString(ilGetError()) << std::endl;
    exit(1);
  }

  ILuint width = ilGetInteger( IL_IMAGE_WIDTH );
  ILuint height = ilGetInteger( IL_IMAGE_HEIGHT );

  uint8_t* buffer = new uint8_t[ width * height * 4 ];

  t.width = width;
  t.height = height;
  t.bytes_per_pixel = 4;
  t.buffer = buffer;

  ilCopyPixels(0, 0, 0, width, height, 1, IL_RGBA, IL_UNSIGNED_BYTE, buffer);

  ilDeleteImage(image);
  ilBindImage(0);

  return t;
}

std::string Material::getTextureTypeString(texture_t type) {
  switch (type) {
  case DIFFUSE_TEXTURE:
    return "DIFFUSE_TEXTURE";
  case SPECULAR_TEXTURE:
    return "SPECULAR_TEXTURE";
  case AMBIENT_TEXTURE:
    return "AMBIENT_TEXTURE";
  case EMISSIVE_TEXTURE:
    return "EMISSIVE_TEXTURE";
  case HEIGHT_TEXTURE:
    return "HEIGHT_TEXTURE";
  case NORMALS_TEXTURE:
    return "NORMALS_TEXTURE";
  case SHININESS_TEXTURE:
    return "SHININESS_TEXTURE";
  case OPACITY_TEXTURE:
    return "OPACITY_TEXTURE";
  case DISPLACEMENT_TEXTURE:
    return "DISPLACEMENT_TEXTURE";
  default:
    return "UNKNOWN TYPE";
  }
}
