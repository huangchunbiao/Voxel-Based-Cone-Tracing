#include "scene_object.hpp"

#include <glm/gtx/transform.hpp>
#include <iostream>

SceneObject::SceneObject(GLuint vao, GLint num_indices){
  num_indices_ = num_indices;
  vao_ = vao;
};

void SceneObject::set_texture(TextureType type, Texture2D* texture){
  textures_[type] = texture;
}


void SceneObject::modified_mat(){
   model_is_modified_= true;
}

void SceneObject::update_model_mat(){
  if(model_is_modified_){
    model_mat_ = translation_mat_ * scale_mat_ * rotation_mat_;
  }
};


glm::mat4 SceneObject::get_model_mat(){
  update_model_mat();
  return model_mat_;
}

void SceneObject::global_rotate(float angle, glm::vec3 pivot){
  rotation_mat_ = glm::rotate(rotation_mat_, angle, pivot);
  x_dir = rotation_mat_ * glm::vec4(x_dir, 1);
  y_dir = rotation_mat_ * glm::vec4(y_dir, 1);
  z_dir = rotation_mat_ * glm::vec4(z_dir, 1);
  modified_mat();
}
void SceneObject::local_rotate(float angle, glm::vec3 pivot){
  glm::vec3 corrected_pivot = rotation_mat_ * glm::vec4(pivot, 1);
  rotation_mat_ = glm::rotate(rotation_mat_, angle, corrected_pivot);
  x_dir = rotation_mat_ * glm::vec4(x_dir, 1);
  y_dir = rotation_mat_ * glm::vec4(y_dir, 1);
  z_dir = rotation_mat_ * glm::vec4(z_dir, 1);
  modified_mat();
}

void SceneObject::rotate_x(float angle){
  rotation_mat_ = glm::rotate(rotation_mat_, angle, x_dir);
  y_dir = rotation_mat_ * glm::vec4(y_dir, 1);
  z_dir = rotation_mat_ * glm::vec4(z_dir, 1);
  modified_mat();
}
void SceneObject::rotate_y(float angle){
  rotation_mat_ =  glm::rotate(rotation_mat_, angle, y_dir);
  x_dir = rotation_mat_ * glm::vec4(x_dir,1);
  z_dir = rotation_mat_ * glm::vec4(z_dir,1);
  modified_mat();
}
void SceneObject::rotate_z(float angle){
  rotation_mat_ =  glm::rotate(rotation_mat_, angle, z_dir);
  y_dir = rotation_mat_ * glm::vec4(y_dir, 1);
  x_dir = rotation_mat_ * glm::vec4(x_dir, 1);
  modified_mat();
}

void SceneObject::move_to(glm::vec3 v){
  translation_mat_ = glm::translate(v);
  modified_mat();
}
void SceneObject::move_to(float x, float y, float z){
  translation_mat_ = glm::translate(glm::vec3(x,y,z));
  modified_mat();
}

void SceneObject::translate(glm::vec3 v){
  translation_mat_ = glm::translate(translation_mat_, v);
  modified_mat();
}
void SceneObject::translate(float x, float y, float z){
  translate(glm::vec3(x,y,z));
}
void SceneObject::translate_x(float s){
  translate(s,0,0);
}
void SceneObject::translate_y(float s){
  translate(0,s,0);
}
void SceneObject::translate_z(float s){
  translate(0,0,s);
}

void SceneObject::scale(glm::vec3 v){
  glm::scale(scale_mat_, v);
  modified_mat();
}
void SceneObject::scale(float x, float y, float z){
  scale(glm::vec3(x,y,z));
}
void SceneObject::scale_x(float x){
  scale(x,1,1);
}
void SceneObject::scale_y(float y){
  scale(1,y,1);
}
void SceneObject::scale_z(float z){
  scale(1,1,z);
}

void SceneObject::size(glm::vec3 v){
  scale_mat_ = glm::scale(v);
  modified_mat();
}
void SceneObject::size(float s){
  size(glm::vec3(s,s,s));
}

void SceneObject::size(float x, float y, float z){
  size(glm::vec3(x,y,z));
}

void SceneObject::bind_vao(){
  //std::cout << "Binding vao\n";
  glBindVertexArray(vao_);
}

void SceneObject::bind_texture(TextureType type, GLuint location){
  Texture2D* texture = textures_[type];
  if(texture != nullptr) {
    texture->bind(location);
  } else {
    glActiveTexture(GL_TEXTURE0 + location);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
}

void SceneObject::bind_textures(GLuint binds[NUM_TEXTURE_TYPES]){
  for(int i = 0; i < NUM_TEXTURE_TYPES; i++){
    if(textures_[i] != nullptr)
      textures_[i]->bind(binds[i]);
  }
}

void SceneObject::draw(){
  glDrawElements(GL_TRIANGLES, num_indices_, GL_UNSIGNED_INT, (void*)0);
}

void SceneObject::run_behaviours(){
  auto itr = behaviours_.begin();

  while (itr != behaviours_.end()) {
    bool res = (*itr)(this);
    if(!res)
      behaviours_.erase(itr);
    itr++;
  }
}

// Returns SceneObject::an int of the behaviours
void SceneObject::add_behaviour(std::function<bool(SceneObject*)> behaviour){
  behaviours_.push_back(behaviour);
}

void SceneObject::clear_behaviours(){
  behaviours_.clear();
}
