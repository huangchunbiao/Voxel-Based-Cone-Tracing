#pragma once

#include <vector>

#include <assimp/scene.h>

class Mesh {
public:
  std::string name_;
  int materialIndex_ = 0;
  
  Mesh(aiMesh* mesh);
  ~Mesh();

  bool hasNormals();
  bool hasPositions();
  bool hasTangents();
  bool hasFaces();

  unsigned int numFaces();
  unsigned int numVertices();
  
  std::vector<float>& getVertices();
  std::vector<int>& getIndices();
  std::vector<float>& getTextureCoords();
  std::vector<float>& getNormals();
  std::vector<float>& getTangents();
  std::vector<float>& getBitangents();
  
  int getMaterialIndex();
  
private:
  bool hasNormals_;
  bool hasPositions_;
  bool hasTangentsAndBitangents_;
  bool hasFaces_;
  
  unsigned int numFaces_;
  unsigned int numVertices_;
  
  std::vector<float> vertices_;  
  std::vector<int> indices_;
  std::vector<float> normals_;
  std::vector<float> tangents_;
  std::vector<float> bitangents_;
  std::vector<float> textureCoords_;  
};
