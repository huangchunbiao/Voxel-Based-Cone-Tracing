#include <cassert>
#include <iostream>

#include "mesh.hpp"

Mesh::Mesh(aiMesh* mesh) {
  std::string n(mesh->mName.C_Str());
  name_ = n;
  materialIndex_ = mesh->mMaterialIndex;

  hasNormals_ = mesh->HasNormals();
  hasPositions_ = mesh->HasPositions();
  hasTangentsAndBitangents_ = mesh->HasTangentsAndBitangents();
  hasFaces_ = mesh->HasFaces();
  
  numFaces_ = mesh->mNumFaces;
  numVertices_ = mesh->mNumVertices;

  vertices_.resize( mesh->mNumVertices * 3 );
  indices_.resize( mesh->mNumFaces * 3 );
  normals_.resize( mesh->mNumVertices * 3 );
  tangents_.resize( mesh->mNumVertices * 3 );
  bitangents_.resize( mesh->mNumVertices * 3 );
  textureCoords_.resize(mesh->mNumVertices * 2);

  for (int i = 0; i < mesh->mNumVertices; i++) {
    aiVector3D vertex = mesh->mVertices[i];
    vertices_[i*3]   = vertex.x;
    vertices_[i*3+1] = vertex.y;
    vertices_[i*3+2] = vertex.z;
  }

  for (int i = 0; i < mesh->mNumFaces; i++) {
    aiFace face = mesh->mFaces[i];
    assert(face.mNumIndices == 3);
    
    indices_[i*3]   = face.mIndices[0];
    indices_[i*3+1] = face.mIndices[1];
    indices_[i*3+2] = face.mIndices[2];
  }
  

  for (int i = 0; i < mesh->mNumVertices; i++) {
    aiVector3D normal = mesh->mNormals[i];
    normals_[i*3]   = normal.x;
    normals_[i*3+1] = normal.y;
    normals_[i*3+2] = normal.z;
  }

  assert(mesh->HasTangentsAndBitangents() && mesh->mTangents != NULL);
  for (int i = 0; i < mesh->mNumVertices; i++) {
    aiVector3D tangent = mesh->mTangents[i];
    assert(!(tangent.x == 0 && tangent.y == 0 && tangent.z == 0));
    tangents_[i*3]   = tangent.x;
    tangents_[i*3+1] = tangent.y;
    tangents_[i*3+2] = tangent.z;
  }

  for (int i = 0; i < mesh->mNumVertices; i++) {
    aiVector3D bitangent = mesh->mBitangents[i];
    bitangents_[i*3]   = bitangent.x;
    bitangents_[i*3+1] = bitangent.y;
    bitangents_[i*3+2] = bitangent.z;
  }

  for (int i = 0; i < mesh->mNumVertices; i++) {
    aiVector3D uv = mesh->mTextureCoords[0][i];
    textureCoords_[i*2]   = uv.x;
    textureCoords_[i*2+1] = uv.y;
  }
}

Mesh::~Mesh() {

}

bool Mesh::hasNormals() {
  return hasNormals_;
}

bool Mesh::hasPositions() {
  return hasPositions_;
}

bool Mesh::hasFaces() {
  return hasFaces_;
}

unsigned int Mesh::numFaces() {
  return numFaces_;
}

unsigned int Mesh::numVertices() {
  return numVertices_;
}

std::vector<float>& Mesh::getVertices() {
  return vertices_;
}

std::vector<int>& Mesh::getIndices() {  
  return indices_;
}

std::vector<float>& Mesh::getTextureCoords() {
  return textureCoords_;
}

std::vector<float>& Mesh::getNormals() {
  return normals_;
}

std::vector<float>& Mesh::getTangents() {
  return tangents_;
}

std::vector<float>& Mesh::getBitangents() {
  return bitangents_;
}

int Mesh::getMaterialIndex() {
  return materialIndex_;
}

