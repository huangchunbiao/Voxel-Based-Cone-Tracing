#include <string>
#include <iostream>

#include "scene.hpp"
#include "world_data.hpp"

#include "../util/gl/color_texture.hpp"

Scene::Scene(std::string word_object_fp){
  world_data_ = new World_Data(word_object_fp);
}

Texture2D* Scene::create_and_store_texture(TextureStore& tex_store, RawTexture texture, std::string texture_name) {

  Texture2D* t = tex_store.get(texture_name);
  if (t == nullptr) {
    t = new Texture2D(texture.width, texture.height, GL_RGBA, GL_RGBA,
		      GL_UNSIGNED_BYTE, texture.buffer);
    t->set_wrap_s(GL_REPEAT);
    t->set_wrap_t(GL_REPEAT);
    t->set_min_filter(GL_LINEAR_MIPMAP_LINEAR);
    t->set_mag_filter(GL_LINEAR);
    t->gen_mipmaps();

    bool success = tex_store.add(texture_name, t);
    if(!success){
      std::cerr <<"FAILED TO STORE TEXTURE: " << texture_name << std::endl;
      exit(1);
    }
  }

  return t;
}

bool Scene::load_world(TextureStore& tex_store){
  if(world_data_ == nullptr){
    error_msg_ = "SCENCE: World objects not initialzed or allready loaded";
    return false;
  }
  for(Mesh mesh : world_data_->get_meshes()){
    auto material_idx = mesh.getMaterialIndex();
    Material material = world_data_->get_materials()[material_idx];
    ShapeData sd = {
      mesh.getVertices(),
      mesh.getNormals(),
      mesh.getTextureCoords(),
      mesh.getTangents(),
      mesh.getBitangents(),
      mesh.getIndices()
    };
    GLData gl_data = Shape::to_gl_data(&sd, 0, 1, 2, 3, 4);
    SceneObject* so = new SceneObject(gl_data.vao, mesh.getIndices().size());

    if(material.hasTexture(DIFFUSE_TEXTURE)){
      std::string texture_name = material.get_texture_name(DIFFUSE_TEXTURE);
      RawTexture texture = material.getTexture(DIFFUSE_TEXTURE);
      Texture2D* diffuse_texture = create_and_store_texture(tex_store, texture, texture_name);
      so->set_texture(SceneObject::DIFFUSE_TEXTURE, diffuse_texture);
    } else {
      so->set_texture(SceneObject::DIFFUSE_TEXTURE, nullptr);
    }

    if(material.hasTexture(HEIGHT_TEXTURE)) {
      std::string texture_name = material.get_texture_name(HEIGHT_TEXTURE);
      RawTexture texture = material.getTexture(HEIGHT_TEXTURE);
      Texture2D* normals_texture = create_and_store_texture(tex_store, texture, texture_name);
      so->set_texture(SceneObject::NORMALS_TEXTURE, normals_texture);
    } else {
      so->set_texture(SceneObject::NORMALS_TEXTURE, nullptr);
    }

    if(material.hasTexture(SPECULAR_TEXTURE)) {
      std::string texture_name = material.get_texture_name(SPECULAR_TEXTURE);
      RawTexture texture = material.getTexture(SPECULAR_TEXTURE);
      Texture2D* specular_texture = create_and_store_texture(tex_store, texture, texture_name);
      so->set_texture(SceneObject::SPECULAR_TEXTURE, specular_texture);
    } else {
      so->set_texture(SceneObject::SPECULAR_TEXTURE, nullptr);
    }

    static_objects_.push_back(so);
  }

  world_data_ = nullptr;
  std::cout << "World objects loaded" << std::endl;

  return true;
}


bool Scene::init(TextureStore& tex_store){
  return load_world(tex_store);

}

void Scene::add_light_source(LightSource ls) {
  switch (ls.type()) {
  case POINT_LIGHT:
    //point_light_sources_.push_back((PointLightSource) ls);
    break;
  case DIRECTIONAL_LIGHT:
    //directional_light_sources_.push_back((DirectionalLightSource) ls);
    break;
  default:
    assert(false);
  }
}

void Scene::add_dyamic_object(SceneObject* o){
  dynamic_objects_.push_back(o);
  has_changes_ = true;
}


void Scene::update_objects(){
  for(auto o : dynamic_objects_){
    o->run_behaviours();
  }
}
