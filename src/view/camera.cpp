#include "camera.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <cassert>

Camera::Camera(){

}

Camera::Camera(float fovy_angle, float far_plane, float near_plane) {
  fovy_angle_ = fovy_angle;
  far_plane_ = far_plane;
  near_plane_ = near_plane;
}

Camera::Camera(glm::vec3 look_at, glm::vec3 position, glm::vec3 up) {
  look_at_ = look_at;
  position_ = position;
  up_ = up;
}

Camera::Camera(glm::vec3 look_at, glm::vec3 position, glm::vec3 up, float fovy_angle,
	       float far_plane, float near_plane) {
  Camera(look_at, position, up);
  Camera(fovy_angle, far_plane, near_plane);

  assert(fovy_angle_ == fovy_angle);
  assert(position == position_);
}

void Camera::horizontal_rotate(float angle) {
  glm::vec3 rot_vec = glm::cross(look_at_, up_);
  look_at_ = glm::rotate(look_at_, angle,  rot_vec);
  up_ = glm::rotate(up_, angle, rot_vec);
}

void Camera::vertical_rotate(float angle){
  glm::vec3 rot_vec(0., 1., 0.);
  look_at_ = glm::rotate(look_at_, angle, rot_vec);
  up_ = glm::rotate(up_, angle, rot_vec);
}

void Camera::move_forward(float step){
  position_ += look_at_ * step;
}

void Camera::move_sideways(float step){
  position_ += glm::cross(look_at_, up_) * step;
}

void Camera::move_up(float step){
  position_ += glm::vec3(0,1,0) * step;
}

void Camera::set_look_at(float x, float y, float z){
  set_look_at(glm::vec3(x,y,z));
}
void Camera::set_look_at(glm::vec3 look_at){
  look_at_ = look_at;
}

glm::mat4 Camera::look_at_matrix() {
  return glm::lookAtRH(position_, look_at_ + position_, up_);
}

glm::mat4 Camera::perspective_matrix() {
  return glm::perspective(glm::radians(fovy_angle_), aspect_ratio_, near_plane_, far_plane_);
}
