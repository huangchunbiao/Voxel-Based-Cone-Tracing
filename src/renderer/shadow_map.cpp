#include "shadow_map.hpp"

#include <glm/gtc/matrix_transform.hpp>

Shadow_Map::Shadow_Map(){

}

Shadow_Map::Shadow_Map(int shadow_texture_width, int shadow_texture_height,
	     glm::vec3 light_look_at, glm::vec3 light_source_position){
  shadow_width_ = shadow_texture_width;
  shadow_height_ = shadow_texture_height;
  light_look_at_ = light_look_at;
  light_source_position_ = light_source_position;
  calculate_light_space_matrix();
}

void Shadow_Map::calculate_light_space_matrix(){
  glm::mat4 perspective =  glm::ortho(-3000.0, 3000.0,
				      -2000.0, 2000.0,
				      1.0, 3000.0
				      );
  glm::mat4 look_at = glm::lookAt(light_source_position_,
				  light_look_at_,
				  glm::vec3(0,0,-1.0));

  light_space_matrix_ = perspective * look_at;
}

void Shadow_Map::set_position(glm::vec3 v){
  light_source_position_ = v;
  calculate_light_space_matrix();
};

bool Shadow_Map::init(){
  int gl_error = GL_NO_ERROR;
  shadow_program_ = Program("shaders/shadow/global_shadow.vert",
			    "shaders/shadow/global_shadow.frag");
  if(!shadow_program_.linked()){
    error_msg_ = "Shadow_Map: Shadow program filed to link_\n   "
	       + shadow_program_.get_error_msg();
    return false;
  }

  visualization_program_ = Program("shaders/shadow/shadow_visualize.vert",
				   "shaders/shadow/shadow_visualize.frag");

  if(!visualization_program_.linked()){
    error_msg_ = "Shadow_Map: visualisaiton program failed to link:\n   "
	       + visualization_program_.get_error_msg();
    return false;
  }

  if(!quad_vao_.init()){
    error_msg_ = "Shadow_Map: quad failed to initialize:\n    "
	       + quad_vao_.get_error_msg();
    return false;
  }


  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "SHADOW_MAP: gl returned error when creating framebuffer " + std::to_string(gl_error);
    return false;
  }
  glGenFramebuffers(1, &depth_map_framebuffer_);

  glGenTextures(1, &depth_map_);
  glBindTexture(GL_TEXTURE_2D, depth_map_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadow_width_, shadow_height_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glBindFramebuffer(GL_FRAMEBUFFER, depth_map_framebuffer_);
  //glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_map_, 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_map_, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);

  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
    error_msg_ = "Shadow frame buffer didn't load";
    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  visualization_program_.use();
  visualization_program_.set_uniform1i("depth_map", 0);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "Depth buffer failed to be created " + std::to_string(gl_error);
    return false;
  }
  std::cerr << "Got here\n";
  return true;
}

void Shadow_Map::bind_framebuffer(){
  glViewport(0, 0, shadow_width_, shadow_height_);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, depth_map_framebuffer_);
}

void Shadow_Map::render(Scene& scene){
  bind_framebuffer();
  glEnable(GL_DEPTH_TEST);
  glClear(GL_DEPTH_BUFFER_BIT);
  shadow_program_.use();
  shadow_program_.set_uniform_matrix4f("light_space_matrix",
				       get_light_space_matrix());
  shadow_program_.set_uniform_matrix4f("model_mat", glm::mat4(1.));
  for( auto o : scene.get_static_objects()){
    o->bind_vao();
    o->draw();
  }
  for(auto o : scene.get_dynamic_objects()){
    o->bind_vao();
    shadow_program_.set_uniform_matrix4f("model_mat", o->get_model_mat());
    o->draw();
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  //glEnable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
}


void Shadow_Map::bind_texture(GLuint layout){
  glActiveTexture(GL_TEXTURE0 + layout);
  glBindTexture(GL_TEXTURE_2D, depth_map_);
}

void Shadow_Map::visualize(){
  visualization_program_.use();
  bind_texture(0);
  quad_vao_.draw();
}
