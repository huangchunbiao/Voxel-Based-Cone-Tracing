#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include "../util/gl/program.hpp"
#include "quad_vao.hpp"
#include "../scene/scene.hpp"
#include "../view/camera.hpp"

class GBuf {
public:
  enum RenderTarget {
    POSITION,
    NORMAL,
    ALBEDO_SPEC,
    TANGENT,
    EMISSION,
    NUM_GBUF_TARGETS
  };
private:
  Program gbuf_program_;
  int render_width_, render_height_;
  GLuint g_buffer_ = 0;
  GLuint textures[NUM_GBUF_TARGETS] = {0};
  GLuint render_buffer_ = 0;

  Quad_VAO quad_vao_;

  std::string error_msg_;
public:
  GBuf();
  GBuf(int render_width, int render_height);
  bool load_shaders();
  bool load_render_targets();


  bool init();
  void render(Camera& camera, Scene& scene);

  void blit();

  void bind_texture(RenderTarget, GLuint locatio);
  void bind_position(GLuint location){ bind_texture(POSITION, location); }
  void bind_normal(GLuint location){ bind_texture(NORMAL, location); }
  void bind_albedo_spec(GLuint location){ bind_texture(ALBEDO_SPEC,location); }
  void bind_tangent(GLuint location){ bind_texture(TANGENT,location); }
  void bind_emission(GLuint location){ bind_texture(EMISSION,location); }

  void unbind_textures();

  std::string get_error_msg(){return error_msg_;}
};
