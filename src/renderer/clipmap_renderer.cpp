#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>
#include <glm/glm.hpp>

#include <cstdlib>

#include "clipmap_renderer.hpp"

ClipmapRenderer::ClipmapRenderer() : ClipmapRenderer(600,600) {

}

ClipmapRenderer::ClipmapRenderer(int render_width, int render_height) : Renderer() {


  clipmap_levels_ = 6;
  voxel_resolution_ = 128;
  voxel_base_dimension_ = 800;

  voxelizer_ = ClipmapVoxelizer(clipmap_levels_, voxel_resolution_, voxel_base_dimension_);

  shadow_map_ = Shadow_Map(3024,3024,
			   glm::vec3(0,0,0),
			   glm::vec3(0,2000,250));
  gbuf_ = GBuf(render_width, render_height);

}

bool ClipmapRenderer::load_shaders(){

  lighting_program_ = Program("shaders/vbct/finalize.vert",
			      "shaders/vbct/clipmap_finalize.frag");
  if(!lighting_program_.linked()){
    error_msg_ = "Lighting program failed: "
	       + lighting_program_.get_error_msg();
    return false;
  }

  return true;
}


bool ClipmapRenderer::init(){

  if(!quad_vao_.init()){
    error_msg_ = quad_vao_.get_error_msg();
    return false;
  }
  if(!voxelizer_.init()){
    error_msg_ = voxelizer_.get_error_msg();
    return false;
  }
  if(!shadow_map_.init()){
    error_msg_ = shadow_map_.get_error_msg();
    return false;
  }

  if(!load_shaders()){
    return false;
  }
  if(!gbuf_.init()){
    error_msg_ = gbuf_.get_error_msg();
    return false;
  }
  return true;
}

void ClipmapRenderer::render(Camera& camera, Scene& scene){
  shadow_map_.render(scene);
  voxelizer_.voxelize(scene, camera, shadow_map_);

  gbuf_.render(camera, scene);
  gbuf_.blit();
  render_lighting(camera);
  // write to default framebuffer
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}


void ClipmapRenderer::render_lighting(Camera& camera){

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0,0, window_width_,window_height_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  lighting_program_.use();

  //lighting_program_.set_uniform_matrix4f("voxel_projection",
  //voxelizer_.get_voxel_projection());
  lighting_program_.set_uniform3f("camera_position",
				  camera.get_position_vec());
  lighting_program_.set_uniform3f("camera_up",
				  camera.get_up_vec());
  lighting_program_.set_uniform3f("camera_lookat",
				  camera.get_look_at_vec());
  lighting_program_.set_uniform_matrix4f("light_projection",
					 shadow_map_.get_light_space_matrix());


  lighting_program_.set_uniform1i("voxel_resolution", voxel_resolution_);
  lighting_program_.set_uniform1i("clipmap_levels", clipmap_levels_);
  lighting_program_.set_uniform1i("voxel_base_dimension", voxel_base_dimension_);

  gbuf_.bind_position(0);
  lighting_program_.set_uniform1i("tex_position", 0);

  gbuf_.bind_normal(1);
  lighting_program_.set_uniform1i("tex_normal", 1);

  gbuf_.bind_albedo_spec(2);
  lighting_program_.set_uniform1i("tex_albedo_spec", 2);

  gbuf_.bind_tangent(3);
  lighting_program_.set_uniform1i("tex_tangent", 3);

  
  shadow_map_.bind_texture(4);
  lighting_program_.set_uniform1i("tex_shadow_map", 4);

  voxelizer_.get_texture().bind(5);
  lighting_program_.set_uniform1i("voxel_field", 5);

  gbuf_.bind_emission(6);
  lighting_program_.set_uniform1i("tex_emission", 6);

  lighting_program_.set_uniform1i("useAO", (int) use_AO_);
  lighting_program_.set_uniform2f("resolution", glm::vec2(window_width_, window_height_));

  quad_vao_.draw();

  glBindTexture(GL_TEXTURE_2D, 0);
}
