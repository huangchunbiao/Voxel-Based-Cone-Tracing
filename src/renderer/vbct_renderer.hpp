#pragma once

#include <GL/gl.h>
#include "renderer.hpp"
#include "util/gl/program.hpp"
#include "quad_vao.hpp"
#include "voxelizer.hpp"
#include "shadow_map.hpp"
#include "voxelization/octree.hpp"
#include "voxelization/voxel_list.hpp"

class VBCT_Renderer : public Renderer{
private:
  const int VOXEL_FIELD_SIZE = 512;

  Program voxelization_program_;
  Program gbuf_program_;
  Program lighting_program_;

  int render_width_, render_height_;

  GLuint voxel_texture_ = 0;
  GLuint g_buffer_ = 0;
  GLuint g_position_ = 0;
  GLuint g_normal_ = 0;
  GLuint g_albedo_spec_ = 0;
  GLuint render_buffer_ = 0;


  Quad_VAO quad_vao_;
  Voxelizer voxelizer_;
  Shadow_Map shadow_map_;

  VoxelList voxel_list_;
  Octree octree_;

  glm::mat4 voxel_projection_ = glm::ortho(-2048.0, 2048.0,
				 -2048.0, 2048.0,
				 2048.0, -2048.0);
  bool load_shaders();
  bool load_render_targets();

  void render_gbuf(Camera& camera, Scene& scene);
  void render_lighting(Camera& camera);

public:
  VBCT_Renderer();
  VBCT_Renderer(int render_width, int render_height);

  void voxelize();
  virtual bool init();
  virtual void render(Camera& camera, Scene& scene);
  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow * window){}
};
