#include "renderer.hpp"
#include <iostream>

Renderer::Renderer(){
}


void Renderer::update_window_dimentions(){
  window_dimentions_ = (float)window_width_ / (float)window_width_;
  //glViewport(0,0,window_width_, window_height_);
}
void Renderer::set_window_width(int width){
  window_width_ = width;
  update_window_dimentions();
}

void Renderer::set_window_height(int height){
  window_height_ = height;
  update_window_dimentions();
}

void Renderer::set_window_dimentions(int width, int height){
  window_width_ = width;
  window_height_ = height;
  update_window_dimentions();
}


void Renderer::set_camera(const Camera* cam){
  camera_ = cam;
}

void Renderer::toggle_AO() {
  use_AO_ = !use_AO_;
}
