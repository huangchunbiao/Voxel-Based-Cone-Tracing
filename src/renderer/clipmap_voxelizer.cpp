#include "clipmap_voxelizer.hpp"

ClipmapVoxelizer::ClipmapVoxelizer(int levels) {
  resolution_ = DEFAULT_FIELD_RESOLUTION;
  dimensions_ = DEFAULT_DIMENSIONS;
  levels_ = levels;

}
ClipmapVoxelizer::ClipmapVoxelizer(int levels, float resolution, float dimensions) {
  levels_ = levels;
  resolution_ = resolution;
  dimensions_ = dimensions;
}

bool ClipmapVoxelizer::init() {
  GLuint gl_error = GL_NO_ERROR;
  voxel_program_ = Program("shaders/voxelizer/clipmap_voxelizer.vert",
			   "shaders/voxelizer/clipmap_voxelizer.geom",
			   "shaders/voxelizer/clipmap_voxelizer.frag");

  if(!voxel_program_.linked()){
    std::cerr << "VOXELIZER: Voxelizer program failed to link: "
	      << voxel_program_.get_error_msg()
	      << std::endl;
    return false;
  }

  voxel_mip_program_ = Program("shaders/vbct/clipmap_mip.comp");
  if (!voxel_mip_program_.linked()) {
    std::cerr << "VOXELIZER: Failed to create compute program: clipmap_mip"
	      << voxel_mip_program_.get_error_msg()
	      << std::endl;
    return false;
  }

  //The x dimension will be higher in case of anisotrophic
  voxel_texture_= Texture3D(resolution_, (resolution_ + 2) * levels_, resolution_,
			    GL_R32UI, 0);
  voxel_texture_.set_wrap(GL_CLAMP_TO_BORDER);
  voxel_texture_.set_min_mag(GL_NEAREST,  GL_NEAREST);

  gl_error = glGetError();
  if(gl_error!= GL_NO_ERROR){
    std::cerr << "CLIPMAP VOXELIZER: Couldn't generate voxel texture. GL ERROR: "
	      << std::to_string(gl_error)
	      << std::endl;
    return false;
  }

  //This texture will be the output texture for the compute shader and used for VBCT
  voxel_texture_final_ = Texture3D(resolution_, (resolution_ + 2) * levels_, resolution_,
				    GL_RGBA8, 0);
  voxel_texture_final_.set_wrap(GL_CLAMP_TO_BORDER);
  voxel_texture_final_.set_min_mag(GL_LINEAR, GL_LINEAR);
  //voxel_texture_final_.set_min_mag(GL_NEAREST, GL_NEAREST);

  gl_error = glGetError();
  if(gl_error!= GL_NO_ERROR){
    std::cerr << "CLIPMAP VOXELIZER: Couldn't generate voxel texture. GL ERROR: "
	      << std::to_string(gl_error)
	      << std::endl;
    return false;
  }

  return true;
}

void ClipmapVoxelizer::voxelize(Scene& scene, Camera& camera, Shadow_Map& shadow_map) {
  voxel_texture_.clear();
  voxel_texture_final_.clear();

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glViewport(0, 0, resolution_, resolution_);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  voxel_texture_.bind(0);
  voxel_program_.set_uniform1i("voxel_field",0);
  voxel_texture_.bind_image_texture(0);

  voxel_program_.set_uniform1i("resolution", resolution_);
  voxel_program_.set_uniform1i("clipmap_levels", levels_);

  voxel_program_.set_uniform1i("diffuse", 0);
  voxel_program_.set_uniform1i("normal_map", 1);

  voxel_program_.set_uniform1i("shadow_map", 2);
  shadow_map.bind_texture(2);
  voxel_program_.set_uniform_matrix4f("shadow_space",shadow_map.get_light_space_matrix());


  glm::vec3 cam_pos = camera.get_position_vec();
  voxel_program_.set_uniform3f("camera_pos", cam_pos);

  glViewport(0, 0, resolution_, resolution_);

  for (int i = 0; i < levels_; i++) {
    voxel_program_.set_uniform1i("level", i);
    float off = dimensions_ * pow(2, i) / 2.0f;

    float m = pow(2, i) * (float) dimensions_ / resolution_;
    cam_pos -= glm::mod(cam_pos, m);

    auto projection = glm::ortho(cam_pos.x - off, cam_pos.x + off,
				 cam_pos.y - off, cam_pos.y + off,
				 -cam_pos.z + off, -cam_pos.z - off);


    voxel_program_.set_uniform_matrix4f("projection",  projection);
    voxel_program_.set_uniform_matrix4f("model", glm::mat4(1.0f));
    voxel_program_.use();
    int e = 0;
    for ( auto object : scene.get_static_objects()){
      voxel_program_.set_uniform3f("emission", object->getEmission());
      object->bind_vao();
      object->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
      object->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
      object->draw();
    }

    for ( auto object : scene.get_dynamic_objects()) {
      voxel_program_.set_uniform3f("emission", object->getEmission());
      voxel_program_.set_uniform_matrix4f("model", object->get_model_mat());
      object->bind_vao();
      object->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
      object->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
      object->draw();
    }
  }

  // voxel_program_.set_uniform_matrix4f("light_projection",
  //				      sm.get_light_space_matrix());


  // sm.bind_texture(GL_TEXTURE1);
  // voxel_program_.set_uniform1i("shadow_map", 1);

  //voxel_texture_.gen_mipmaps();

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  voxel_mip_program_.use();

  voxel_texture_.bind(0);
  voxel_mip_program_.set_uniform1i("voxel_field_out",0);
  voxel_texture_.bind_image_texture(0);

  voxel_texture_final_.bind(1);
  voxel_mip_program_.set_uniform1i("voxel_field_out",1);
  voxel_texture_final_.bind_image_texture(1);

  glDispatchCompute(resolution_, (resolution_ + 2) * levels_, resolution_);
  glUseProgram(0);
}
