#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>
#include <glm/glm.hpp>

#include <cstdlib>

#include "simple_renderer.hpp"
#include "renderer.hpp"


SimpleRenderer::SimpleRenderer() : Renderer(){
  voxelizer_ = Voxelizer(512, glm::vec3(2048, 2048, 2048));
}


bool SimpleRenderer::init(){
  if(!voxelizer_.init()){
    error_msg_ = "SIMPLE RENDERER: voxelizer failed to init: "
	       + voxelizer_.get_error_msg();
    return false;
  }

  program_ = Program("shaders/default.vs", "shaders/default.fs");
  if(!program_.linked()){
    std::cerr << program_.get_error_msg() << std::endl;
    return false;
  }


  shadow_map_ = new Shadow_Map(1024,1024,
			       glm::vec3(0,0,0),
			       glm::vec3(0,1500,250));
  if(!shadow_map_->init()){
    std::cerr << shadow_map_->get_error_msg() << std::endl;
    return false;
  }
  return true;
}

void SimpleRenderer::set_window_dimentions(int width, int height){
  voxelizer_.set_window_dimentions(width, height);
  window_width_ = width;
  window_height_ = height;
}

void SimpleRenderer::render(Camera& camera, Scene& scene){


  if(scene.has_changes()){
    shadow_map_->render(scene);
    voxelizer_.voxelize(scene, *shadow_map_);

    scene.set_has_changes(false);

  }

  glViewport(0, 0, window_width_, window_height_);


  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);


  //shadow_map_->visualize();
  //return;
  voxelizer_.visualize(camera);
  return;


  srand(0);
  glm::mat4 perspective = camera.perspective_matrix();

  glm::mat4 look_at = camera.look_at_matrix();
  program_.set_uniform_matrix4f("projection_mat", perspective);
  program_.set_uniform_matrix4f("view_mat", look_at);
  program_.set_uniform_matrix4f("light_space_matrix",
				 shadow_map_->get_light_space_matrix());
  shadow_map_->bind_texture(GL_TEXTURE4);
  program_.set_uniform1i("shadow_map", 4);

  program_.use();
  for( auto o : scene.get_static_objects()){
    o->bind_vao();
    o->bind_textures({0});
    o->draw();
  }

  // for( auto vao : scene.get_world_vaos()){
  //   vao.bind();
  //   float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  //   float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  //   float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
  //   program_.set_uniform3f("col", r, g, b);
  //   vao.draw(program_);
  // }
}
