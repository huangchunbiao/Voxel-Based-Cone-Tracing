#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <sstream>
#include <cstdlib>


#include "octree_renderer.hpp"


OctreeRenderer::OctreeRenderer() : OctreeRenderer(600,600){

}
OctreeRenderer::OctreeRenderer(int render_width, int render_height) : Renderer(){
  // Note to sellf (bug) if sizes are > 1600 the injection of shadows doesn't allways work.
  shadow_map_ = Shadow_Map(3000,3000,
			   glm::vec3(0,0,0),
			   glm::vec3(0,2000,250));

  // Octree with nine levels (2 ^ 9) == 512
  octree_ = Octree(9);

  // Inital tests need 16777216 bytes of data for static scene
  voxel_list_ = VoxelList(2 << 26);

  gbuf_ = GBuf(render_width, render_height);
}

bool OctreeRenderer::load_shaders(){
  finalize_program_ = Program("shaders/voxelizer/octree/vbct/finalize.vert",
			      "shaders/voxelizer/octree/vbct/finalize.frag");
  if(!finalize_program_.linked()){
    error_msg_ = "OCTREE Renderer: Finalize program failed: "
	       + finalize_program_.get_error_msg();
    return false;
  }

  if(!voxel_list_.init()){
    error_msg_ = "OCTREE Renderer: \n" + voxel_list_.get_error_msg();
    return false;
  }
  if(!octree_.init()){
    error_msg_ = "OCTREE Renderer:\n" + octree_.get_error_msg();
    return false;
  }

  return true;
}


bool OctreeRenderer::init(){

  if(!quad_vao_.init()){
    error_msg_ = quad_vao_.get_error_msg();
    return false;
  }

  if(!shadow_map_.init()){
    error_msg_ = shadow_map_.get_error_msg();
    return false;
  }

  if(!load_shaders()){
    return false;
  }
  if(!gbuf_.init()){
    error_msg_ = "OCTREE Renderer: Failed to init GBuf\n"
	       + gbuf_.get_error_msg();
    return false;
  }
  return true;
}

void OctreeRenderer::render(Camera& camera, Scene& scene){

  if(scene.has_changes()){
    shadow_map_.render(scene);
    voxel_list_.clear();

    voxel_list_.render(scene.get_static_objects(),
		       voxel_projection_,
		       512);

    octree_.construct_static(voxel_list_);
    octree_.inject_light(shadow_map_, voxel_projection_);
    octree_.finnish_static();

    //std::cout << "Construction done" << std::endl;
    scene.set_has_changes(false);
  }
  // static bool i = true;
  // if(i){
  //   shadow_map_.render(scene);

  //   voxel_list_.clear();
  //   voxel_list_.render(scene.get_dynamic_objects(),
  //		       voxel_projection_,
  //		       512);
  //   octree_.construct_dynamic(voxel_list_);
  //   octree_.inject_light(shadow_map_, voxel_projection_);
  //   octree_.finnish_dynamic();
  // }
  //i = !i;

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0,0,window_width_,window_height_);
  octree_.visualize(camera, window_width_, window_height_, voxel_projection_);
  return;

  gbuf_.render(camera, scene);
  gbuf_.blit();

  render_finalize(camera);
  // write to default framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

}



void OctreeRenderer::render_finalize(Camera& camera){

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0,0, window_width_,window_height_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  finalize_program_.use();

  finalize_program_.set_uniform1i("voxel_span", 512);

  finalize_program_.set_uniform_matrix4f("voxel_projection",
					 voxel_projection_);
  finalize_program_.set_uniform3f("camera_position",
				  camera.get_position_vec());
  finalize_program_.set_uniform_matrix4f("light_projection",
					 shadow_map_.get_light_space_matrix());

  finalize_program_.set_uniform3i("voxel_bricks_dims",
				  octree_.get_voxel_brick_dims());

  octree_.bind_octree_nodes(0);

  octree_.bind_voxel_bricks(0);
  finalize_program_.set_uniform1i("voxel_bricks", 0);

  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr << "GL ERROR WHILE RENDERING FINAL IMAGE " << gl_error << std::endl;
    exit(1);
  }

  gbuf_.bind_position(1);
  finalize_program_.set_uniform1i("tex_position", 1);

  gbuf_.bind_normal(2);
  finalize_program_.set_uniform1i("tex_normal", 2);

  gbuf_.bind_albedo_spec(3);
  finalize_program_.set_uniform1i("tex_albedo_spec", 3);
  gbuf_.bind_tangent(4);
  finalize_program_.set_uniform1i("tex_tangent", 4);

  shadow_map_.bind_texture(5);
  finalize_program_.set_uniform1i("tex_shadow_map", 5);

  quad_vao_.draw();

}



std::string OctreeRenderer::performance_report(Camera& camera, Scene& scene, GLFWwindow* window){
  std::ostringstream report;
  // Rendering shadows
  std::cout << "Testing shadows" << std::endl;
  {
    long time = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      shadow_map_.render(scene);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Shadow map: " << (double)time / (double)samples << std::endl;
  }

  // Rendering static voxel list
    std::cout << "Static voxelization" << std::endl;
  {
    long time = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      voxel_list_.clear();
      voxel_list_.render(scene.get_static_objects(),
			 voxel_projection_,
			 512);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Static voxel list construction: " << (double)time / (double)samples << std::endl;
  }
  // Inserting static voxels
  std::cout << "Static voxel construction" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      voxel_list_.clear();

      voxel_list_.render(scene.get_static_objects(),
			 voxel_projection_,
			 512);
      octree_.clear();
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.construct(voxel_list_, true);
      octree_.node_count_.checkpoint();
      octree_.save_counts(octree_.static_level_counts_);
      octree_.level_counts_ = octree_.static_level_counts_;
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static octree construction: " << (double)time / (double)samples << std::endl;
  }
    // Inserting static voxels
  std::cout << "Static neighbor finder" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      voxel_list_.clear();

      voxel_list_.render(scene.get_static_objects(),
			 voxel_projection_,
			 512);
      octree_.clear();
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.neighbor_finder(false);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static neighbor finding: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Static radiance injection" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      auto start =  std::chrono::high_resolution_clock::now();
      octree_.radiance_inject(1, false);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static radiance injection: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Static radiance filter" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      auto start =  std::chrono::high_resolution_clock::now();
      octree_.radiance_filter(1, false);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static radiance filter: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Static radiance mipmap insert" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      auto start =  std::chrono::high_resolution_clock::now();
      for(int level = 2; level < 9; level++){
	octree_.mipmap(level, false);
      }
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static mipmap insert: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Static radiance mipmap filter" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){

      auto start =  std::chrono::high_resolution_clock::now();
      for(int level = 2; level < 9; level++){
	octree_.mipmap_neighbor_filter(level, false);
      }
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

    }
    report << "Static mipmap filter: " << (double)time / (double)samples << std::endl;
  }



  // Rendering dynamic voxel list
  std::cout << "Dynamic voxelization" << std::endl;
  {
    long time = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      voxel_list_.clear();
      voxel_list_.render(scene.get_dynamic_objects(),
			 voxel_projection_,
			 512);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Dynamic voxel list construction: " << (double)time / (double)samples << std::endl;
  }


  // Rendering dynamic voxel list
  std::cout << "Dynamic octree construction:" << std::endl;
  {
    long time = 0;
    long dyn_time  = 0;
    long light_time = 0;
    long neighbor_time = 0;
    int samples = 1000;
    for(int i = 0; i < samples; i++){
      scene.update_objects();
      auto dyn_start =  std::chrono::high_resolution_clock::now();
      octree_.clear_dynamic();
      auto dyn_finish = std::chrono::high_resolution_clock::now();
      dyn_time +=  std::chrono::duration_cast<std::chrono::milliseconds>(dyn_finish-dyn_start).count();
      auto light_start = std::chrono::high_resolution_clock::now();
      octree_.clear_light();
      auto light_finish = std::chrono::high_resolution_clock::now();
      light_time +=  std::chrono::duration_cast<std::chrono::milliseconds>(light_finish-light_start).count();


      voxel_list_.clear();
      voxel_list_.render(scene.get_dynamic_objects(),
			 voxel_projection_,
			 512);
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.construct(voxel_list_, false);
      octree_.save_counts(octree_.level_counts_);
      auto finish = std::chrono::high_resolution_clock::now();
       time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();

      auto neighbor_start =  std::chrono::high_resolution_clock::now();
      octree_.neighbor_finder(false);
      auto neighbor_finish = std::chrono::high_resolution_clock::now();
      neighbor_time +=  std::chrono::duration_cast<std::chrono::milliseconds>(neighbor_finish-neighbor_start).count();


    }
    report << "Dynamic octree insertion: " << (double)time / (double)samples << std::endl;

    report << "Dynamic clear voxels: " << (double)dyn_time / (double)samples << std::endl;
    report << "Dynamic clear light: " << (double)light_time / (double)samples << std::endl;
    report << "Dynamic neighbor finder: " << (double)neighbor_time / (double)samples << std::endl;
  }

  // Injecting light
  std::cout << "Injecting light" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.inject_light(shadow_map_, voxel_projection_);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Injecting light: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Dynamic radiance inject" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.radiance_inject(1, true);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Radiance inject: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Dynamic radiance filter" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      octree_.radiance_filter(1, true);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Dynamic radiance inject: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Dynamic mipmap insert" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      for(int level = 2; level < 9; level++){
	octree_.mipmap(level, true);
      }
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Dynamic mipmap insert: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Dynamic mipmap insert" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      for(int level = 2; level < 9; level++){
	octree_.mipmap_neighbor_filter(level, true);
      }
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Dynamic mipmap insert: " << (double)time / (double)samples << std::endl;
  }

  std::cout << "Presenting final image" << std::endl;
  {
    long time = 0;
    int samples = 100;
    for(int i = 0; i < samples; i++){
      auto start =  std::chrono::high_resolution_clock::now();
      glClearColor(0.0, 0.0, 0.0, 1.0);
      glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);


      gbuf_.render(camera, scene);
      gbuf_.blit();

      render_finalize(camera);
      // write to default framebuffer
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      glfwSwapBuffers(window);
      auto finish = std::chrono::high_resolution_clock::now();
      time +=  std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count();
    }
    report << "Presenting final image: " << (double)time / (double)samples << std::endl;
  }

  return report.str();
}
