#include "octree.hpp"
#include <cmath>
#include <algorithm>

#include <glm/gtc/matrix_inverse.hpp>
#define NUM_INITIAL_NODES 9

Octree::Octree(int max_depth){
  static_level_counts_ = std::vector<GLuint>(10,0);
  level_counts_ = std::vector<GLuint>(10,0);

  tree_levels_ = max_depth;
  voxel_field_side_length_ = pow(2, max_depth);
  max_num_voxels_ = voxel_field_side_length_ * voxel_field_side_length_ * voxel_field_side_length_;
  max_num_octree_nodes_ = pow(8, max_depth);

  // Arbitary numer, replace with true math
  octree_buffer_size_ = 3800000 * 8 * 4 * 2;

  voxel_brick_dims_ = glm::ivec3(256, 256, 256);
  voxel_brick_buffer_size_ = 3 * voxel_brick_dims_;
}

bool Octree::init(){
  GLuint gl_error = GL_NO_ERROR;

  octree_program_ = Program("shaders/voxelizer/octree/octree.comp");
  if(!octree_program_.linked()){
    error_msg_ = "OCTREE: Failed to link octree construct program\n" + octree_program_.get_error_msg();
    return false;
  }

  mipmap_program_ = Program("shaders/voxelizer/octree/new_mipmap.comp");
  if(!mipmap_program_.linked()){
    error_msg_ = "OCTREE: Failed to link mipmap program\n" + mipmap_program_ .get_error_msg();
    return false;
  }

  mipmap_neighbor_program_ = Program("shaders/voxelizer/octree/mipmap_neighbor_filter.comp");
  if(!mipmap_neighbor_program_.linked()){
    error_msg_ = "OCTREE: Failed to link mipmap filter program" + mipmap_neighbor_program_.get_error_msg();
    return false;
  }

  // Cube render program
  trace_program_ = Program("shaders/voxelizer/octree/v_trace.vert",
			  "shaders/voxelizer/octree/v_trace.frag");
  if(!trace_program_.linked()){
    error_msg_ = "Voxel trace program failed: "
	       + trace_program_.get_error_msg();
    return false;
  }

  radiance_inject_program_= Program("shaders/voxelizer/octree/radiance_inject.comp");
  if(!radiance_inject_program_.linked()){
    error_msg_ = "OCTREE: Failed to link radiance injection program\n"
	       + radiance_inject_program_.get_error_msg();
    return false;
  }

  radiance_filter_program_= Program("shaders/voxelizer/octree/radiance_filter.comp");
  if(!radiance_filter_program_.linked()){
    error_msg_ = "OCTREE: Failed to link radiance filter program\n"
	       + radiance_filter_program_.get_error_msg();
    return false;
  }

  neighbor_finder_program_ = Program("shaders/voxelizer/octree/neighbor_finder.comp");
  if(!neighbor_finder_program_.linked()){
    error_msg_ = "OCTREE: Failed to link neighbor finder program\n"
	       + neighbor_finder_program_.get_error_msg();
    return false;
  }
  light_inject_program_ = Program("shaders/voxelizer/octree/light_inject.vert",
				  "shaders/voxelizer/octree/light_inject.frag");
  if(!light_inject_program_.linked()){
    error_msg_ = "OCTREE: Failed to link light injection program\n"
	       + light_inject_program_.get_error_msg();
    return false;
  }

  clear_program_ = Program("shaders/voxelizer/octree/clear.comp");
  if(!clear_program_.linked()){
    error_msg_ = "OCTREE: Faield to link clear program" + clear_program_.get_error_msg();
    return false;
  }

  clear_light_program_ = Program("shaders/voxelizer/octree/clear.comp");
  if(!clear_light_program_.linked()){
    error_msg_ = "OCTREE: Faield to link clear program" + clear_light_program_.get_error_msg();
    return false;
  }


  if(!quad_vao_.init()){
    error_msg_ = "OCTREE: Quad vao failed \n"
	       + quad_vao_.get_error_msg();
    return false;
  }

  // 9 since root node and its (non initialized) children are present from the start
  node_count_ = AtomicCounter(NUM_INITIAL_NODES);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "OCTREE: Failed to create atomic counter: GL error: "
	       + std::to_string(gl_error);
    return false;
  }

  octree_nodes_ = ShaderStorage(octree_buffer_size_);
  initialize_node_data();
  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "OCTREE: Failed to create shader storage. GL error: "
	       + std::to_string(gl_error);
    return false;
  }


  // The size of the buffer depends on how many ellements should be
  // buffered each layer this is currently set to a aseries of pretty
  // narbitary numbers
  octree_level_count_ = ShaderStorage((2235801+20) * 4);
  initialize_level_count();
  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "OCTREE: Failed to create octree level count storage buffer";
    return false;
  }

  // Voxel bricks buffer, uses same counter as the octree for buffering
  // Currently set to an arbitary number we need rougly 3.9 milion texure bricks
  // The static scene has around 2 milion voxels at the lowest level.
  //  (128 * 8) * (16 * 8) * (16 * 8)

  voxel_bricks_ = Texture3D(voxel_brick_buffer_size_.x,
			    voxel_brick_buffer_size_.y,
			    voxel_brick_buffer_size_.z,
			    texture_type_, 1);
  voxel_bricks_.set_wrap(GL_CLAMP_TO_BORDER);
  voxel_bricks_.set_min_mag(GL_LINEAR,
			    GL_LINEAR);
  voxel_bricks_.clear();
  gl_error = glGetError();
  if(gl_error!= GL_NO_ERROR){
    error_msg_ = "OCTREE: Couldn't generate voxel texture. GL ERROR: "
	       + std::to_string(gl_error);
    return false;
  }

  return true;
}

void Octree::initialize_node_data(){
    // Initializes space for the root element and its children
  GLuint buffer[8 * NUM_INITIAL_NODES] = {0};
  // 1 for pointing to its children which will be 1
  // 1 << 31 simply is the static bit
  buffer[0] = 1 | (1 << 31);
  buffer[8] = 0;

  octree_nodes_.buffer_sub_data(0, sizeof(buffer), buffer);
}

void Octree::initialize_level_count(){
    // Ten first elements are for the counters The ten after that are
  // static offset numbers.  Followed by the initialisation of the
  // position of the root element.
  GLuint buffer[20 + 9] =  {0};
  /// Level 9 contains one element from the start
  buffer[9] = 1;
  memcpy(&buffer[10], level_offsets_, 10 * sizeof(GLuint));

  // The root node has node index 0;
  buffer[20] = 0;
  octree_level_count_.buffer_sub_data(0, sizeof(buffer),
				      buffer);
}



void Octree::mipmap(int level, bool skip_static){
  mipmap_program_.use();
  octree_nodes_.bind(0);
  voxel_bricks_.bind_image(1, texture_type_);
  octree_level_count_.bind(2);
  mipmap_program_.set_uniform1i("current_level", level);
  mipmap_program_.set_uniform3i("voxel_bricks_dims", voxel_brick_dims_);
  mipmap_program_.set_uniform1i("skip_static", skip_static);
  glDispatchCompute(640, 1, 1);
  reset_counts(level_counts_);
  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr << "MIPMAPPING FUCKED UP" << std::endl;
    exit(1);
  }
}


void Octree::mipmap_neighbor_filter(int level, bool skip_static){
  mipmap_neighbor_program_.use();
  octree_nodes_.bind(0);
  voxel_bricks_.bind_image(1, texture_type_);
  octree_level_count_.bind(2);
  mipmap_neighbor_program_.set_uniform3i("voxel_bricks_dims", voxel_brick_dims_);
  mipmap_neighbor_program_.set_uniform1i("current_level", level);
  mipmap_neighbor_program_.set_uniform1i("skip_static", skip_static);
  for(int dir = 0; dir < 3; dir++){
    // 0 is right and left. 1 is up and down. 2 is front and back
    mipmap_neighbor_program_.set_uniform1i("dir_idx", dir);
    glDispatchCompute(640, 1, 1);
    reset_counts(level_counts_);
  }
  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr<< "OCTREE: Failed to run neghbor filter program" << std::endl;
    exit(1);
  }
}

void Octree::construct(VoxelList& vl, bool is_static){
  // Starts at level 8 since level 9 is created at init
  octree_program_.use();
  vl.bind_list(0);
  vl.bind_counter(1);

  octree_nodes_.bind(2);
  node_count_.bind(3);
  octree_level_count_.bind(4);
  octree_program_.set_uniform1i("voxel_span", 512);
  octree_program_.set_uniform1i("is_static", is_static);
  glDispatchCompute(640,1,1);
}

void Octree::finnish(bool skip_static){
  // Injects the colors to the bottom layer
  radiance_inject(1, skip_static);

  radiance_filter(1, skip_static);

  if(!skip_static){
  for(int level = 2; level < 9; level++){
    mipmap(level, skip_static);
    mipmap_neighbor_filter(level, skip_static);
  }
  }
}

void Octree::inject_light(Shadow_Map& sm, glm::mat4 voxel_projection){
  // Injects the colors to the bottom layer
  light_inject_program_.use();
  sm.bind_framebuffer();
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  octree_nodes_.bind(0);
  sm.bind_texture(0);
  light_inject_program_.set_uniform1i("depth_map", 0);

  light_inject_program_.set_uniform1i("voxel_span", 512);
  light_inject_program_.set_uniform_matrix4f("light_proj_inverse",
					     glm::affineInverse(sm.get_light_space_matrix()));
  light_inject_program_.set_uniform_matrix4f("voxel_projection",
					     voxel_projection);

  light_inject_program_.use();
  quad_vao_.bind();
  quad_vao_.draw();

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  GLuint gl_error = GL_NO_ERROR;
  if(gl_error != GL_NO_ERROR){
    std::cerr << "FUCKED UP LIGHT INJECT" << std::endl;
    exit(1);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Octree::construct_static(VoxelList& vl){
  construct(vl, true);
  node_count_.checkpoint();
  save_counts(static_level_counts_);
  level_counts_ = static_level_counts_;
  neighbor_finder(false);
}
void Octree::construct_dynamic(VoxelList& vl){
  clear_dynamic();
  clear_light();
  construct(vl, false);
  save_counts(level_counts_);
  neighbor_finder(true);
}

void Octree::finnish_static(){
  finnish(false);
}
void Octree::finnish_dynamic(){
  finnish(true);
}

void Octree::clear_dynamic(){
  reset_counts(level_counts_);

  clear_program_.use();
  octree_nodes_.bind(0);
  octree_level_count_.bind(1);
  for(int level = 1; level < 9; level++){
    clear_program_.set_uniform1i("current_level", level);
    clear_program_.set_uniform1i("static_start", static_level_counts_[level]);
    glDispatchCompute(640,1,1);
  }

  level_counts_ = static_level_counts_;
  reset_counts(level_counts_);
  node_count_.reset();
}

void Octree::clear_light(){
  clear_light_program_.use();
  octree_nodes_.bind(0);
  octree_level_count_.bind(1);
  clear_program_.set_uniform1i("current_level", 0);
  reset_counts(level_counts_);
}

void Octree::neighbor_finder(bool skip_static){
  neighbor_finder_program_.use();
  octree_nodes_.bind(0);
  octree_level_count_.bind(1);
  neighbor_finder_program_.set_uniform1i("skip_static", skip_static);
  for(int level = 8; level >= 1;level--){
    neighbor_finder_program_.set_uniform1i("current_level", level);
    int num_threads = 640;
    glDispatchCompute(num_threads,1,1);
    reset_counts(level_counts_);
  }
}

void Octree::radiance_inject(int level, bool skip_static){
  radiance_inject_program_.use();
  octree_nodes_.bind(0);
  voxel_bricks_.bind_image(1, texture_type_);
  octree_level_count_.bind(2);

  radiance_inject_program_.use();
  radiance_inject_program_.set_uniform3i("voxel_bricks_dims", voxel_brick_dims_);
  radiance_inject_program_.set_uniform1i("current_level", level);
  radiance_inject_program_.set_uniform1i("skip_static", skip_static);
  glDispatchCompute(640, 1, 1);
  reset_counts(level_counts_);
  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr<< "OCTREE: Failed to run color injection program" << std::endl;
    exit(1);
  }
}

void Octree::radiance_filter(int level, bool skip_static){
  radiance_filter_program_.use();
  octree_nodes_.bind(0);
  voxel_bricks_.bind_image(1, texture_type_);
  octree_level_count_.bind(2);

  radiance_filter_program_.use();
  radiance_filter_program_.set_uniform3i("voxel_bricks_dims", voxel_brick_dims_);
  radiance_filter_program_.set_uniform1i("current_level", level);
  radiance_filter_program_.set_uniform1i("skip_static", skip_static);
  for(int dir = 0; dir < 3; dir++){
    // 0 is right and left. 1 is up and down. 2 is front and back
    radiance_filter_program_.set_uniform1i("dir_idx", dir);
    glDispatchCompute(640, 1, 1);
    reset_counts(level_counts_);
  }
  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr<< "OCTREE: Failed to run neghbor filter program" << std::endl;
    exit(1);
  }
}

int Octree::get_num_octree_nodes(){
  return node_count_.get();
}

void Octree::visualize( Camera& camera, int window_width, int window_height, glm::mat4 voxel_projection){
  trace_program_.use();
  trace_program_.set_uniform1f("fov_tan", 1.0);
  trace_program_.set_uniform1f("aspect_ratio", 16.0/9.0);

  trace_program_.set_uniform2f("resolution", glm::vec2(window_width, window_height));
  trace_program_.set_uniform3f("world_box", 2048, 2048, 2048);

  trace_program_.set_uniform3i("voxel_bricks_dims", voxel_brick_dims_);
  trace_program_.set_uniform1i("voxel_span", 512);
  trace_program_.set_uniform_matrix4f("voxel_projection", voxel_projection);

  octree_nodes_.bind(0);
  //voxel_bricks_.bind_image(1, texture_type_);
  voxel_bricks_.bind(0);
  trace_program_.set_uniform1i("voxel_bricks", 0);

  trace_program_.set_uniform3f("camera_pos", camera.get_position_vec());
  trace_program_.set_uniform3f("camera_dir", camera.get_look_at_vec());
  trace_program_.set_uniform3f("camera_up", camera.get_up_vec());
  quad_vao_.draw();
  voxel_bricks_.unbind();

}


void Octree::print_level_count(){
  GLint* count;
  octree_level_count_.bind_buffer();

  count = (GLint*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
				    0,
				    10 * sizeof(GLint),
				    GL_MAP_READ_BIT
				    );
  std::cout << "Octree level count:\n";
  for(int i = 9; i >= 0; i--){
    std::cout << "Level: " << i << " " << count[i] << std::endl;
  }
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  octree_level_count_.unbind_buffer();
}

void Octree::print_node_data(int offset, int count){
  GLuint* buffer;
  octree_nodes_.bind_buffer();

  buffer = (GLuint*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
				     offset * sizeof(GLuint) * 8,
				     count * sizeof(GLuint) * 8,
				     GL_MAP_READ_BIT
				     );
  std::cout << "Octree Data:\n";
  for(int i = 0; i < count; i++){
    std::cout << "Node nr: " << i << std::endl;
    for(int n = 0; n < 8; n++){
      std::cout << buffer[(offset * 8) + (i *8) + n] << std::endl;
    }
  }
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  octree_nodes_.unbind_buffer();

}
void Octree::save_counts(std::vector<GLuint>& buffer){
  GLint* count;
  octree_level_count_.bind_buffer();

  count = (GLint*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
				    0,
				    10 * sizeof(GLint),
				    GL_MAP_READ_BIT
				    );
  //std::cout << "Saving staticounts:\n";
  for(int i = 0; i < buffer.size(); i++){
    buffer[i] = count[i];
    //std::cout << i << ": " << count[i] << std::endl;
  }
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  octree_level_count_.unbind_buffer();
}

void Octree::reset_counts(std::vector<GLuint>& buffer){
  GLint* count;
  octree_level_count_.bind_buffer();

  count = (GLint*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
				    0,
				    10 *sizeof(GLint),
				    GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT
				    );
  for(int i = 0; i < buffer.size(); i++){
    count[i] = buffer[i];
  }
  glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 10 * sizeof(GLuint));

  GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr <<  "FAILED TO WRITE TO LEVEL BUFFER " << gl_error;
    exit(1);
  }
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  octree_level_count_.unbind_buffer();
}

void Octree::clear_level_count(std::vector<GLuint>& buffer){
  for(int i = 0; i < buffer.size(); i++){
    buffer[i] = 0;
  }
  reset_counts(buffer);
}


void Octree::clear(){
  node_count_.set(NUM_INITIAL_NODES);
  node_count_.checkpoint();

  initialize_node_data();
  initialize_level_count();

  static_level_counts_ = std::vector<GLuint>(static_level_counts_.size(), 0);
  level_counts_ = std::vector<GLuint>(static_level_counts_.size(), 0);

}
