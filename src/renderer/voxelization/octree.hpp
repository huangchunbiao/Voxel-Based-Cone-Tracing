#pragma once

#include <string>

#include <GL/glew.h>
#include <GL/gl.h>
#include <vector>

#include "voxel_list.hpp"
#include "../shadow_map.hpp"

#include "../../util/gl/shader_storage.hpp"
#include "../../util/gl/atomic_counter.hpp"
#include "../../util/gl/texture3d.hpp"
#include "../../util/gl/program.hpp"
#include "../../renderer/quad_vao.hpp"
#include "../../view/camera.hpp"

class Octree {
private:
   // Remove after performance testing
public:
  std::string error_msg_;
  // Size in texels of the brick texture
  glm::ivec3 voxel_brick_buffer_size_;
  // Number of bricks in every direction
  // Every brick has 3³ texels
  glm::ivec3 voxel_brick_dims_;

  GLuint  texture_type_ = GL_RGBA8;

  Program octree_program_;
  Program neighbor_finder_program_;

  Program light_inject_program_;

  Program radiance_inject_program_;
  Program radiance_filter_program_;

  Program mipmap_program_;
  Program mipmap_neighbor_program_;

  Program trace_program_;

  Program clear_program_;
  Program clear_light_program_;

  ShaderStorage octree_nodes_;
  ShaderStorage octree_level_count_;


  std::vector<GLuint> static_level_counts_;
  std::vector<GLuint> level_counts_;

  GLuint level_offsets_[10] = {0,172649,43113,10729,2633,585,73,9,1,0};

  AtomicCounter node_count_;
  Texture3D voxel_bricks_;

  Quad_VAO quad_vao_;

  int tree_levels_;
  int voxel_field_side_length_;
  int max_num_voxels_;
  int max_num_octree_nodes_;
  int octree_node_size = 8 * 4;
  int octree_buffer_size_;
  int octree_num_static_nodes_ = 0;


  void mipmap(int level, bool skip_static);
  void mipmap_neighbor_filter(int level, bool skip_static);

  void construct(VoxelList& vl, bool is_static);
  void finnish(bool skip_static);

  void save_counts(std::vector<GLuint>& buffer);
  void reset_counts(std::vector<GLuint>& buffer);
  void clear_level_count(std::vector<GLuint>& buffer);

  void radiance_inject(int level, bool skip_static);
  void radiance_filter(int level, bool skip_static);
  void neighbor_finder(bool skip_static);

  void clear_dynamic();
  void clear_light();

  void initialize_node_data();
  void initialize_level_count();

public:
  Octree(){};
  Octree(int max_depth);
  ~Octree(){};
  bool init();

  void construct_static(VoxelList& vl);
  void construct_dynamic(VoxelList& vl);

  void finnish_static();
  void finnish_dynamic();

  void inject_light(Shadow_Map& sm, glm::mat4 voxel_projection);


  //void gen_mipmaps(bool skip_static_only = false);
  void visualize(Camera& camera, int window_width, int window_height, glm::mat4 voxel_projection);

  int get_num_octree_nodes();
  std::string get_error_msg(){return error_msg_;}

  void print_node_data(int offset, int count);
  void print_level_count();


  void bind_octree_nodes(GLuint location){  octree_nodes_.bind(location);}
  void bind_voxel_bricks(GLuint location) { voxel_bricks_.bind(location);}

  Texture3D* get_voxel_bricks(){return &voxel_bricks_;}

  glm::ivec3 get_voxel_brick_buffer_size(){ return voxel_brick_buffer_size_;}
  glm::ivec3 get_voxel_brick_dims(){ return voxel_brick_dims_;}

  // Clears the contets of the voxel list
  void clear();

};
