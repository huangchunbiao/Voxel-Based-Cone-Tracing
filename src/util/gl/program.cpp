#include "program.hpp"

#include <iostream>


Program::Program(){

}

Program::Program(Shader& compute){
  program_id_= glCreateProgram();
  glAttachShader(program_id_, compute.get_glid());
  link_program();
}

Program::Program(Shader& vertex, Shader& fragment){
  program_id_= glCreateProgram();
  glAttachShader(program_id_, vertex.get_glid());
  glAttachShader(program_id_, fragment.get_glid());
  link_program();
}

Program::Program(Shader& vertex, Shader& geometry, Shader& fragment){
  program_id_= glCreateProgram();
  glAttachShader(program_id_, vertex.get_glid());
  glAttachShader(program_id_, geometry.get_glid());
  glAttachShader(program_id_, fragment.get_glid());
  link_program();
}


Program::Program(std::string compute_fp){
  program_id_ = glCreateProgram();
  Shader compute(compute_fp, GL_COMPUTE_SHADER);
  if(!compute.compiled()){
    error_msg_ = compute.get_error_msg();
    linked_ = false;
    return;
  }
  glAttachShader(program_id_, compute.get_glid());
  link_program();
}

Program::Program(std::string vertex_fp, std::string fragment_fp){
  program_id_= glCreateProgram();
  Shader vertex(vertex_fp, GL_VERTEX_SHADER);
  if(!vertex.compiled()){
    error_msg_ = vertex.get_error_msg();
    linked_ = false;
    return;
  }
  Shader fragment(fragment_fp, GL_FRAGMENT_SHADER);
  if(!fragment.compiled()){
    error_msg_ = fragment.get_error_msg();
    linked_ = false;
    return;
  }
  glAttachShader(program_id_, vertex.get_glid());
  glAttachShader(program_id_, fragment.get_glid());
  link_program();
}

Program::Program(std::string vertex_fp, std::string geometry_fp, std::string fragment_fp){
  program_id_= glCreateProgram();
  Shader vertex(vertex_fp, GL_VERTEX_SHADER);
  if(!vertex.compiled()){
    error_msg_ = vertex.get_error_msg();
    linked_ = false;
    return;
  }

  Shader geometry(geometry_fp, GL_GEOMETRY_SHADER);
  if(!vertex.compiled()){
    error_msg_ = geometry.get_error_msg();
    linked_ = false;
    return;
  }

  Shader fragment(fragment_fp, GL_FRAGMENT_SHADER);
  if(!fragment.compiled()){
    error_msg_ = fragment.get_error_msg();
    linked_ = false;
    return;
  }
  glAttachShader(program_id_, vertex.get_glid());
  glAttachShader(program_id_, geometry.get_glid());
  glAttachShader(program_id_, fragment.get_glid());
  link_program();
}


bool Program::link_program(){
  glLinkProgram(program_id_);
  GLint success;
  glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
  if( !success ){
    linked_ = false;
    char info_log[1024];
    glGetProgramInfoLog(program_id_, 1024, NULL, info_log);
    error_msg_ = info_log;
  }
  return success;
}


int Program::get_uniform_location(std::string name){
  return glGetUniformLocation(program_id_, (GLchar*)name.c_str());
}

GLint Program::get_program(){
  return program_id_;
}

void Program::use(){
  glUseProgram(get_program());
}


bool Program::linked(){
  return linked_;
}

string Program::get_error_msg(){
  return error_msg_;
}


void Program::set_uniform3f(std::string name, float x, float y, float z){
  use();
  glUniform3f(get_uniform_location(name), x, y, z);
}

void Program::set_uniform3f(std::string name, glm::vec3 data){
  use();
  glUniform3fv(get_uniform_location(name), 1, glm::value_ptr(data));
}
void Program::set_uniform3i(std::string name, glm::ivec3 data){
  use();
  glUniform3iv(get_uniform_location(name), 1, glm::value_ptr(data));
}
void  Program::set_uniform2i(std::string name, glm::ivec2 vec){
  use();
  glUniform2iv(get_uniform_location(name), 1, glm::value_ptr(vec));
}
void  Program::set_uniform2f(std::string name, glm::vec2 vec){
  use();
  glUniform2fv(get_uniform_location(name), 1, glm::value_ptr(vec));
}

void Program::set_uniform_matrix4f(std::string name, glm::mat4 matrix) {
  use();
  glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Program::set_uniform1i(std::string name, GLint value){
  use();
  glUniform1i(get_uniform_location(name), value);
}
void Program::set_uniform1i(GLint location, GLint value){
  use();
  glUniform1i(location, value);
}

void Program::set_uniform1ui(std::string name, GLuint value){
  use();
  glUniform1ui(get_uniform_location(name), value);
}


void Program::set_uniform1f(std::string name, GLfloat value){
  use();
  glUniform1f(get_uniform_location(name), value);
}
