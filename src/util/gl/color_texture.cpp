#include "color_texture.hpp"

const glm::vec4 color_vectors[Texture::N_COLORS] = {
    glm::vec4(0,0,0,1),
    glm::vec4(1),
    glm::vec4(1,0,0,1),
    glm::vec4(0,1,0,1),
    glm::vec4(0,0,1,1)
  };

glm::vec4 Texture::to_vec4(Texture::BasicColor color){
  return color_vectors[color];
};



Texture2D* Texture::make_texture2d(BasicColor color){
  return Texture::make_texture2d(Texture::to_vec4(color));
}
Texture2D* Texture::make_texture2d(glm::vec4 color){
  auto t = new Texture2D(1, 1, GL_RGBA, GL_RGBA,
			 GL_FLOAT, glm::value_ptr(color));
  t->set_wrap_s(GL_REPEAT);
  t->set_wrap_t(GL_REPEAT);
  return t;
}
