#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>

#include "shader.hpp"

class Program{
private:
  GLint program_id_;
  bool  linked_ = true;
  std::string error_msg_;



  bool link_program();

public:

  Program(std::string compute_fp);
  Program(std::string vertex_fp, std::string fragment_fp);
  Program(std::string vertex_fp, std::string geometry_fp, std::string fragment_fp);
  Program(Shader& compute);
  Program(Shader& vertex, Shader& fragment);
  Program(Shader& vertex, Shader& geometry, Shader& fragment);
  Program();
  GLint get_program();
  void use();

  bool linked();
  string get_error_msg();

  int get_uniform_location(std::string name);

  void set_uniform1i(std::string name, GLint value);
  void set_uniform1i(GLint location, GLint value);
  void set_uniform1ui(std::string name, GLuint value);
  void set_uniform1f(std::string name, GLfloat value);

  void set_uniform2i(std::string name, glm::ivec2 vec);
  void set_uniform2f(std::string name, glm::vec2 vec);
  void set_uniform3f(std::string name, float x, float y, float z);
  void set_uniform3f(std::string name, glm::vec3);
  void set_uniform3i(std::string name, glm::ivec3);

  void set_uniform_matrix4f(std::string name, glm::mat4 matrix);
};
