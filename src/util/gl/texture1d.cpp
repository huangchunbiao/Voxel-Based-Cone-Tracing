#include "texture1d.hpp"

#include <iostream>


Texture1D::Texture1D(GLuint size, GLuint internal_format){
  internal_format_ = internal_format;
  glGenTextures(1, &buffer_);

  glBindTexture(GL_TEXTURE_1D, buffer_);

  glTexStorage1D( GL_TEXTURE_1D,
		  1,
		  GL_RG32UI,
		  15000);
  glBindTexture(GL_TEXTURE_1D, 0);
}

Texture1D::~Texture1D(){
  //glDeleteBuffers(1, &buffer_);
}

void Texture1D::bind(GLuint layout){
  glActiveTexture(GL_TEXTURE0 + layout);
  glBindTexture(GL_TEXTURE_1D, buffer_);
}

void Texture1D::unbind(){
  glBindTexture(GL_TEXTURE_1D, 0);
}

void Texture1D::bind_image_read(GLuint location){
  bind_image_read(location, internal_format_);
}
void Texture1D::bind_image_read(GLuint location, GLuint format){
  glBindImageTexture(location, buffer_, 0, GL_TRUE, 0, GL_READ_ONLY, format);
}
void Texture1D::bind_image_write(GLuint location){
  bind_image_write(location, internal_format_);
}
void Texture1D::bind_image_write(GLuint location, GLuint format){
    glBindImageTexture(location, buffer_, 0, GL_TRUE, 0, GL_WRITE_ONLY, format);
}
void Texture1D::bind_image(GLuint location){
  bind_image(location, internal_format_);
}
void Texture1D::bind_image(GLuint location, GLuint format){
    glBindImageTexture(location, buffer_, 0, GL_TRUE, 0, GL_READ_WRITE, format);
}
