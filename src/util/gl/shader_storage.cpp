#include "shader_storage.hpp"
#include <cstring>
#include <iostream>

ShaderStorage::ShaderStorage(GLuint size, GLvoid* data){
  glGenBuffers(1, &ssbo_);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glBufferData(GL_SHADER_STORAGE_BUFFER, size, data, GL_DYNAMIC_COPY);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER,0);
}


ShaderStorage::ShaderStorage(GLuint size){
  GLuint clear[] = {0,0,0,0};
  glGenBuffers(1, &ssbo_);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glBufferData(GL_SHADER_STORAGE_BUFFER, size, NULL, GL_DYNAMIC_COPY);



  glClearBufferData(GL_SHADER_STORAGE_BUFFER, GL_R32UI,
		    GL_RED, GL_UNSIGNED_INT, clear);


  glBindBuffer(GL_SHADER_STORAGE_BUFFER,0);
}




void ShaderStorage::bind(GLuint binding){
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, ssbo_);
}

void ShaderStorage::bind_buffer(){
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
}

void ShaderStorage::unbind_buffer(){
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void ShaderStorage::buffer_sub_data(GLintptr offset, GLsizeiptr size, const GLvoid* data){
  GLuint* buffer;
  bind_buffer();
  buffer = (GLuint*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER,
				     offset,
				     size,
				     GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT
				     );

  std::memcpy(buffer, data, size);
  glFlushMappedBufferRange(GL_SHADER_STORAGE_BUFFER, offset, size);
  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
}


void ShaderStorage::clear_subdata(GLintptr offset, GLsizeiptr size){
  GLuint clear[1] = {0};
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glClearBufferSubData(GL_SHADER_STORAGE_BUFFER, GL_R32UI, offset, size,
		       GL_RED, GL_UNSIGNED_INT, clear);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER,0);
  GLuint gl_error = GL_NO_ERROR;
  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr << "CLEAR SUBDTA DONE GOODEF\n";
    exit(1);
  }
}
